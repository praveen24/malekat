-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2018 at 12:36 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `Malekat`
--

-- --------------------------------------------------------

--
-- Table structure for table `accessibility`
--

CREATE TABLE `accessibility` (
  `id` int(11) NOT NULL,
  `en_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ar_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `is_arabic` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accessibility`
--


-- --------------------------------------------------------

--
-- Table structure for table `bathroom`
--

CREATE TABLE `bathroom` (
  `id` int(11) NOT NULL,
  `en_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ar_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `is_arabic` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bathroom`
--


-- --------------------------------------------------------

--
-- Table structure for table `bedroom`
--

CREATE TABLE `bedroom` (
  `id` int(11) NOT NULL,
  `en_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ar_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `is_arabic` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bedroom`
--


-- --------------------------------------------------------

--
-- Table structure for table `kitchen`
--

CREATE TABLE `kitchen` (
  `id` int(11) NOT NULL,
  `en_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ar_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `is_arabic` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kitchen`
--


-- --------------------------------------------------------

--
-- Table structure for table `latest_news`
--

CREATE TABLE `latest_news` (
  `id` int(11) NOT NULL,
  `en_content` text NOT NULL,
  `ar_content` text CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `latest_news`
--


-- --------------------------------------------------------

--
-- Table structure for table `living_area`
--

CREATE TABLE `living_area` (
  `id` int(11) NOT NULL,
  `en_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ar_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `is_arabic` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `living_area`
--


-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE `medias` (
  `id` int(11) NOT NULL,
  `en_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ar_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `is_arabic` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medias`
--


-- --------------------------------------------------------

--
-- Table structure for table `room_accessibility`
--

CREATE TABLE `room_accessibility` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `accessibility_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_accessibility`
--


-- --------------------------------------------------------

--
-- Table structure for table `room_bathroom`
--

CREATE TABLE `room_bathroom` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `bathroom_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_bathroom`
--

-- --------------------------------------------------------

--
-- Table structure for table `room_bedroom`
--

CREATE TABLE `room_bedroom` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `bedroom_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_bedroom`
--


-- --------------------------------------------------------

--
-- Table structure for table `room_kitchen`
--

CREATE TABLE `room_kitchen` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `kitchen_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_kitchen`
--


-- --------------------------------------------------------

--
-- Table structure for table `room_living_area`
--

CREATE TABLE `room_living_area` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `living_area_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_living_area`
--


-- --------------------------------------------------------

--
-- Table structure for table `room_media`
--

CREATE TABLE `room_media` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_media`
--

-- --------------------------------------------------------

--
-- Table structure for table `room_services`
--

CREATE TABLE `room_services` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `services_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_services`
--



-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `en_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ar_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `is_arabic` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `accessibility`
--
ALTER TABLE `accessibility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bathroom`
--
ALTER TABLE `bathroom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bedroom`
--
ALTER TABLE `bedroom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kitchen`
--
ALTER TABLE `kitchen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `latest_news`
--
ALTER TABLE `latest_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `living_area`
--
ALTER TABLE `living_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_accessibility`
--
ALTER TABLE `room_accessibility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_bathroom`
--
ALTER TABLE `room_bathroom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_bedroom`
--
ALTER TABLE `room_bedroom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_kitchen`
--
ALTER TABLE `room_kitchen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_living_area`
--
ALTER TABLE `room_living_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_media`
--
ALTER TABLE `room_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_services`
--
ALTER TABLE `room_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accessibility`
--
ALTER TABLE `accessibility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bathroom`
--
ALTER TABLE `bathroom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bedroom`
--
ALTER TABLE `bedroom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `kitchen`
--
ALTER TABLE `kitchen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `latest_news`
--
ALTER TABLE `latest_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `living_area`
--
ALTER TABLE `living_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `room_accessibility`
--
ALTER TABLE `room_accessibility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `room_bathroom`
--
ALTER TABLE `room_bathroom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `room_bedroom`
--
ALTER TABLE `room_bedroom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `room_kitchen`
--
ALTER TABLE `room_kitchen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `room_living_area`
--
ALTER TABLE `room_living_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `room_media`
--
ALTER TABLE `room_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `room_services`
--
ALTER TABLE `room_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;