
$('#gallery').lightGallery({
    thumbnail:true
}); 
$('#mobile-menu-icon').click(function(){
	$('.mobile-menu').addClass('visible-sidebar');	
});
$('#hidden-bar-closer').click(function(){
	$('.mobile-menu').removeClass('visible-sidebar');
})
var mainHeader = jQuery(".main-header");
jQuery(window).on('scroll', function () {
	jQuery(document).scrollTop() > 30 && mainHeader.addClass("sticky");
	jQuery(document).scrollTop() < 30 && mainHeader.removeClass("sticky");
});

$(".lang_switch").click(function(e){

	e.preventDefault();
	var language = $(this).data('id');
	var current_url = $(this).data('url');

	$.ajax({
	  type: "POST",
	  url: site_url + 'home/manage_language',
	  data: {language : language, current_url : current_url},
	  success: function (data) {
	  	window.location = current_url;
	  }
	});

});
$(document).ready(function(){
  	$('.checkbox').iCheck({
    	checkboxClass: 'icheckbox_square-orange',
    	radioClass: 'iradio_square-orange',
    	increaseArea: '20%' // optional
  	});
});
$('.datepicker').datepicker({
    startDate: '-3d',
    autoclose: true,
});
AOS.init();