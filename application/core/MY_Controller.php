<?php

/**
 * 
 */
class MY_Controller extends CI_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->data = array();
        $this->load->library('pagination');
        $this->load->model('home_model');
        $this->data['menu'] = $this->home_model->getOneWhere(array('status' => 1), 'menu');
        $about = $this->home_model->getAll('about_us');
        $this->data['about_us'] = $about[0];
    }

    //for pagination
protected function paginate($perpage, $total, $base_url, $uri_segment, $class = "") {
    $config['base_url'] = $base_url;
    $config['total_rows'] = $total;
    $config['per_page'] = $perpage;
    $config["uri_segment"] = $uri_segment;
    $config["reuse_query_string"] = TRUE;
    $choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = 4;
    $config['use_page_numbers'] = TRUE;
        //config for bootstrap pagination class integration
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = false;
    $config['last_link'] = false;
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li class="prev">';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>'; 
    $config['last_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    return $this->pagination->create_links();
}

protected function send_mail($to, $to_name, $from='kunhu707@gmail.com', $from_name='Test Name', $subject, $message, $cc = '', $bcc = array(), $attachments = array()) {

    $params = array(
        'to' => $to,
        'to_name' => $to_name,
        'from' => $from,
        'from_name' => $from_name,
        'subject' => $subject,
        'message' => $message,
        'cc' => $cc
        );

    $ch = curl_init("http://162.243.236.231/sendmail_api/mail/index.php/sendMail");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, count($params));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
    $data = curl_exec($ch);
    curl_close($ch);

    return (is_null($data)) ? false : $data;
}

public function response($data = array()){
    header("Content-type:application/json");
    die(json_encode($data));
}

}
