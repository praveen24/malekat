<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function debug($var, $label = 'Debug Data') {
    echo '<h1>' . $label . '</h1><pre>';
    print_r($var);
    echo '</pre>';
    exit();
}

function debug_continue($var, $label = 'Debug Data') {
    echo '<h1>' . $label . '</h1><pre>';
    print_r($var);
    echo '</pre>';
}
function get_youtube_video_id($video_id){

    
    
    // Did we get a URL?
    if ( FALSE !== filter_var( $video_id, FILTER_VALIDATE_URL ) )
    {

        // http://www.youtube.com/v/abcxyz123
        if ( FALSE !== strpos( $video_id, '/v/' ) )
        {
            list( , $video_id ) = explode( '/v/', $video_id );
        }
        // http://www.youtube.com/embed/abcxyz123
        else if( FALSE !== strpos( $video_id, '/embed/' )){
            list( , $video_id ) = explode( '/embed/', $video_id );
        }
        // http://www.youtube.com/watch?v=abcxyz123
        else{
            $video_query = parse_url( $video_id, PHP_URL_QUERY );
            parse_str( $video_query, $video_params );
//            debug_continue($video_params);
            $video_id = $video_params['v'];
        }

    }

    return $video_id;

}


function debug_html($var, $label = 'Debug Data') {
    ?>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <!-- Viewport metatags -->
            <meta name="HandheldFriendly" content="true">
            <meta name="MobileOptimized" content="320">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

            <!-- iOS webapp metatags -->
            <meta name="apple-mobile-web-app-capable" content="yes">
            <meta name="apple-mobile-web-app-status-bar-style" content="black">
            <title>Staffs Portal</title>
            <meta http-equiv="Cache-Control" content="no-cache">
            <meta http-equiv="Pragma" content="no-cache">
            <meta http-equiv="Expires" content="0">
        </head>
        <body>
    <?php
    echo '<h1>' . $label . '</h1><pre>';
    print_r($var);
    echo '</pre>';
    ?>
        </body>
    </html>
    <?php
    exit();
}
