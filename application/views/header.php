<!DOCTYPE html>
<head>
<title>Malekat Admin Panel</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Malekat Admin Panel" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link rel="stylesheet" href="<?= base_url('assets/admin/css/bootstrap.min.css') ?>" >
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="<?= base_url('assets/admin/css/style.css') ?>" rel='stylesheet' type='text/css' />
<link href="<?= base_url('assets/admin/css/style-responsive.css') ?>" rel="stylesheet"/>
<!-- font CSS -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
<link rel="stylesheet" href="<?= base_url('assets/admin/css/font.css') ?>" type="text/css"/>
<link href="<?= base_url('assets/admin/css/font-awesome.css') ?>" rel="stylesheet"> 
<link rel="stylesheet" href="<?= base_url('assets/admin/css/morris.css') ?>" type="text/css"/>
<!-- calendar -->
<link rel="stylesheet" href="<?= base_url('assets/admin/css/monthly.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/bootstrap-tagsinput.css') ?>">
                        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/min/dropzone.min.css">
<!-- //calendar -->
<!-- //font-awesome icons -->
<script src="<?= base_url('assets/admin/js/jquery2.0.3.min.js') ?>"></script>
<script src="<?= base_url('assets/admin/js/raphael-min.js') ?>"></script>
<script src="<?= base_url('assets/admin/js/morris.js') ?>"></script><!--main content end-->
<script src="<?= base_url('assets/admin/js/bootstrap.js') ?>"></script>
<script src="<?= base_url('assets/admin/js/jquery.dcjqaccordion.2.7.js') ?>"></script>
<script src="<?= base_url('assets/admin/js/scripts.js') ?>"></script>
<script src="<?= base_url('assets/admin/js/jquery.slimscroll.js') ?>"></script>
<script src="<?= base_url('assets/admin/js/jquery.nicescroll.js') ?>"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="<?= base_url('assets/admin/js/jquery.scrollTo.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap-tagsinput.js') ?>"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/min/dropzone.min.js?v=3.4.5"></script>
</head>
<body>
<section id="container">
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand" style="background-color: #fff;">
    <a href="<?= site_url('admin/home') ?>" class="logo" style="font-size: 16px;width: 100%;margin: 13px 0 0 0px;">
        <img src="<?= base_url('assets/images/logo.png') ?>" class="img-responsive center-block" style="width: 100px;">
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<!--logo end-->

<div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img alt="" src="<?= base_url('assets/admin/images/2.png') ?>">
                <span class="username"> Admin</span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <li><a href="<?= site_url('admin/change_password') ?>"><i class=" fa fa-suitcase"></i>Change Password</a></li>
                <li><a href="<?= site_url('auth/logout') ?>"><i class="fa fa-key"></i> Log Out</a></li>
            </ul>
        </li>
        <!-- user login dropdown end -->
       
    </ul>
    <!--search & user info end-->
</div>
</header>
<!--header end-->