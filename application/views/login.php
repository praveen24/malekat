<!DOCTYPE html>
    <head>
        <title>Malekat Admin Login Panel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Malekat Admin Login Panel" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- bootstrap-css -->
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <!-- //bootstrap-css -->
        <!-- Custom CSS -->
        <link href="<?= base_url('assets/admin/css/style.css') ?>" rel='stylesheet' type='text/css' />
        <link href="<?= base_url('assets/admin/css/style-responsive.css') ?>" rel="stylesheet"/>
        <!-- font CSS -->
        <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <!-- font-awesome icons -->
        <link rel="stylesheet" href="<?= base_url('assets/admin/css/font.css') ?>" type="text/css"/>
        <link href="<?= base_url('assets/admin/css/font-awesome.css') ?>" rel="stylesheet"> 
        <!-- //font-awesome icons -->
        <script src="<?= base_url('assets/admin/js/jquery2.0.3.min.js') ?>"></script>
    </head>
    <body style="background-image: url(<?= base_url('assets/images/login-bg.jpg') ?>);background-size: cover;padding: 50px; width: 100%;">
    <?php if ($this->session->flashdata('success')) : ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php elseif ($this->session->flashdata('error')) : ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php elseif ($this->session->flashdata('info')) : ?>
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('info'); ?>
                </div>
            <?php endif; ?>
        <div class="log-w3">
            <div class="w3layouts-main">
                <?php if ($message != ''): ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>
                <h2>Sign In Now</h2>
                <form action="" method="post">
                    <input type="text" class="ggg" name="username" placeholder="E-MAIL" required="">
                    <input type="password" class="ggg" name="password" placeholder="PASSWORD" required="">
                    <h6><a href="<?= site_url('auth/forgot_password') ?>" id="forgotpass">Forgot Password?</a></h6>
                    <div class="clearfix"></div>
                    <input type="submit" value="Sign In" name="login">
                </form>
            </div>
        </div>
        <script src="<?= base_url('assets/admin/js/bootstrap.js') ?>"></script>
        <script src="<?= base_url('assets/admin/js/jquery.dcjqaccordion.2.7.js') ?>"></script>
        <script src="<?= base_url('assets/admin/js/scripts.js') ?>"></script>
        <script src="<?= base_url('assets/admin/js/jquery.slimscroll.js') ?>"></script>
        <script src="<?= base_url('assets/admin/js/jquery.nicescroll.js') ?>"></script>
        <script src="<?= base_url('assets/admin/js/jquery.scrollTo.js') ?>"></script>
    </body>
</html>