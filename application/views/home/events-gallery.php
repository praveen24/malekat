<div class="container-fluid">
	<div class="row heading">
		<div class="col-sm-12 text-center">
			<h1><?= $this->session->userdata('arabic') ? 'صور الأحداث' : 'Events Photos' ?></h1>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row gallery-section">
		<div class="container">
			<div class="row">
				<div id="gallery">
					<?php foreach ($gallery as $value) { ?>
					<a href="<?= base_url('uploads/event_gallery/'.$value->image) ?>">
						<div class="col-sm-4" data-aos="fade-up">
							<div class="hover ehover13">
								<img src="<?= base_url('uploads/event_gallery/'.$value->image) ?>" class="img-responsive">
								<div class="overlay">
									<h2><i class="fa fa-search"></i></h2>
									<h3><?= $this->session->userdata('arabic') ? $value->ar_title : $value->en_title ?></h3>
								</div>
							</div>
						</div>
					</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>