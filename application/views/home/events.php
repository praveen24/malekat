<div class="container-fluid">
	<div class="row heading">
		<div class="col-sm-12 text-center">
			<h1><?= $this->session->userdata('arabic') ? 'أحدث الأحداث لدينا' : 'Our Latest Events' ?></h1>
			<h2><?= $this->session->userdata('arabic') ? 'مساحات فاخرة للأحداث لا تنسى' : 'Luxurious Spaces for Memorable events' ?></h2>
		</div>
	</div>
</div>
<div class="container-fluid">
<?php $i=1; foreach ($events as $event) { ?>
	<div class="row main-service <?= $this->session->userdata('arabic') ? 'arabic_service' : '' ?>" data-aos="fade-up">
		<?php if ($i%2 == 0) { ?>
		<div class="col-sm-6 nopadding">
			<div style="background-image: url('<?= base_url('uploads/events/'.$event->image) ?>');background-size: cover;width: 100%;height: 400px;" class="service-bg"></div>
		</div>
		<?php } ?>
		<div class="col-sm-6 pl100 content">
			<h3><?= $this->session->userdata('arabic') ? $event->ar_name : $event->en_name ?></h3>
			<div class="clearfix"></div><br>
			<h4><?= $this->session->userdata('arabic') ? $event->ar_title : $event->en_title ?></h4>
			<p><?= $this->session->userdata('arabic') ? $event->ar_desc : $event->en_desc ?></p>
		</div>
		<?php if ($i%2) { ?>
		<div class="col-sm-6 nopadding">
			<div style="background-image: url('<?= base_url('uploads/events/'.$event->image) ?>');background-size: cover;width: 100%;height: 400px;" class="service-bg"></div>
		</div>
		<?php } ?>
	</div>
	<?php $i++; } ?>
</div>