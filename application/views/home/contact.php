<div class="container-fluid">
	<div class="row heading">
		<div class="col-sm-12 text-center">
			<h1><?= $this->session->userdata('arabic') ? $menu->ar_contact : $menu->en_contact ?></h1>
			<h2><?= $this->session->userdata('arabic') ? 'لا تترددوا في الاتصال بنا إذا كان لديك أي أسئلة أخرى' : 'Do not hesitate to contact us if you have any further questions' ?></h2>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row contact-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php if ($this->session->flashdata('success')) : ?>
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php elseif ($this->session->flashdata('error')) : ?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('error'); ?>
						</div>
					<?php elseif ($this->session->flashdata('info')) : ?>
						<div class="alert alert-info alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('info'); ?>
						</div>
					<?php endif; ?>
				</div>
				<div class="col-sm-8 col-sm-offset-2 <?= $this->session->userdata('arabic') ? 'arabic_contact' : '' ?>">
					<div class="col-sm-7" data-aos="<?= $this->session->userdata('arabic') ? 'fade-left' : 'fade-right' ?>">
						<form action="<?= site_url('contact_info') ?>" method="post">
							<div class="form-group">
								<input type="text" name="name" dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>" placeholder="<?= $this->session->userdata('arabic') ? 'اسم' : 'Name' ?>" class="form-control" required>
							</div>
							<div class="form-group">
								<input type="email" name="email" dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>" placeholder="<?= $this->session->userdata('arabic') ? 'البريد الإلكتروني' : 'Email' ?>" class="form-control" required>
							</div>
							<div class="form-group">
								<input type="text" name="phone" dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>" placeholder="<?= $this->session->userdata('arabic') ? 'رقم الهاتف' : 'Phone Number' ?>" class="form-control" required>
							</div>
							<div class="form-group">
								<textarea name="message" dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>" placeholder="<?= $this->session->userdata('arabic') ? 'رسالة' : 'Message' ?>" class="form-control" rows="5" required></textarea>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-send pull-right"><?= $this->session->userdata('arabic') ? 'إرسال' : 'SEND' ?></button>
							</div>
						</form>
					</div>
					<div class="col-sm-5 address" data-aos="<?= $this->session->userdata('arabic') ? 'fade-right' : 'fade-left' ?>">
						<p><i class="fa fa-map-marker" style="<?= $this->session->userdata('arabic') ? 'margin-left: 10px;' : 'margin-right: 10px;' ?>"></i><?= $this->session->userdata('arabic') ? '٧٨٠٨ عبد الله آل مانياي، أبهور<br>حي الجابرية، جدة<br>١٣٧١٩، المملكة العربية السعودية' : '7808 Abdullah Al Maniaai, <br>Abhur Al Janubiyah District, <br>Jeddah 23719, Saudi Arabia' ?></p>
						<p style="<?= $this->session->userdata('arabic') ? 'direction: initial;float: right;flex-direction: row-reverse;flex-wrap: wrap;' : '' ?>"><i class="fa fa-phone" style="<?= $this->session->userdata('arabic') ? 'margin-left: 10px;' : 'margin-right: 10px;' ?>"></i><?= $this->session->userdata('arabic') ? '+٩٦٦١٢٦٩٩٢٠٢٠' : '+966 12 699 2020' ?></p>
						<p><i class="fa fa-envelope"></i> info@malekat.com</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row contact-section" style="padding-bottom: 0;">
		<div class="col-sm-12 nopadding">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3706.8905040503037!2d39.10278241451846!3d21.706976385634903!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15c3d88ef811c6e7%3A0x90a593a1f3d5c2ae!2s7808+Abdullah+Al+Maniaai%2C+Abhur+Al+Janubiyah+District%2C+Jeddah+23719%C2%A03983%2C+Saudi+Arabia!5e0!3m2!1sen!2sin!4v1514975962504" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
</div>