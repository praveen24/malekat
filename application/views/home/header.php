<header class="main-header">
	<!-- <div class="header-top">
		<div class="container">
			<p>Call Us Now : 920033308</p>
			<ul>
				<li>
					<a href="#"><i class="fa fa-facebook"></i></a>
				</li>
				<li>
					<a href="#"><i class="fa fa-instagram"></i></a>
				</li>
				<li>
					<a href="#"><i class="fa fa-snapchat-ghost"></i></a>
				</li>
				<li>
					<a href="#"><i class="fa fa-twitter"></i></a>
				</li>
				<li>
					<a href="#"><i class="fa fa-youtube-play"></i></a>
				</li>
			</ul>
		</div>
	</div> -->
	<div class="container main-header-cont">
		<nav class="col-md-4 col-lg-4 hidden-sm hidden-xs">
			<div class="logo">
				<a href="<?= site_url() ?>"><img src="<?= base_url('assets/images/logo.png') ?>" class="img-responsive center-block"></a>
			</div>
		</nav>
		<nav class="col-lg-8 col-md-8 hidden-sm hidden-xs nopadding" id="main-menu">
			<ul class="main-menu" style="<?= $this->session->userdata('arabic') ? 'flex-direction: row-reverse;float: left;' : '' ?>">
				<li>
					<a href="<?= site_url() ?>"><?= $this->session->userdata('arabic') ? $menu->ar_home : $menu->en_home ?></a>
				</li>	
				<li>
					<a href="<?= site_url('about') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_about : $menu->en_about ?></a>
				</li>	
				<li>
					<a href="#"><?= $this->session->userdata('arabic') ? $menu->ar_gallery : $menu->en_gallery ?></a>
					<ul class="sub-menu">
						<li>
							<a href="<?= site_url('gallery/hall-photos') ?>"><?= $this->session->userdata('arabic') ? 'صور القاعة' : 'Hall Photos' ?></a>
						</li>
						<li>
							<a href="<?= site_url('gallery/events-photos') ?>"><?= $this->session->userdata('arabic') ? 'معرض الأحداث' : 'Events Gallery' ?></a>
						</li>
						<li>
							<a href="<?= site_url('gallery/video-gallery') ?>"><?= $this->session->userdata('arabic') ? 'معرض الفيديو' : 'Video Gallery' ?></a>
						</li>
					</ul>
				</li>	
				<li>
					<a href="<?= site_url('services') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_services : $menu->en_services ?></a>
				</li>	
				<li>
					<a href="<?= site_url('events') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_events : $menu->en_events ?></a>
				</li>	
				<li>
					<a href="<?= site_url('contact') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_contact : $menu->en_contact ?></a>
				</li>	
				<li>
					<a href="<?= site_url('reservation') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_reservation : $menu->en_reservation ?></a>
				</li>
				<li><a href="#" class="lang_switch" data-url="<?= current_url() ?>" data-id="<?= $this->session->userdata('arabic') ? 'arabic' : 'english' ?>"><?= $this->session->userdata('arabic') ? 'English' : 'عربى' ?></a></li>
			</ul>
		</nav>
		<div class="col-xs-6 hidden-md hidden-lg">
			<div class="logo">
				<a href="<?= site_url() ?>"><img src="<?= base_url('assets/images/logo.png') ?>" class="center-block"></a>
			</div>
		</div>
		<div class="col-xs-6 hidden-md hidden-lg">
			<span id="mobile-menu-icon" class="pull-right"><i class="fa fa-bars"></i></span>
		</div>
		<nav class="mobile-menu">
			<span class="fa fa-close pull-right" id="hidden-bar-closer"></span>
			<ul>
				<li>
					<a href="<?= site_url() ?>"><?= $this->session->userdata('arabic') ? $menu->ar_home : $menu->en_home ?></a>
				</li>	
				<li>
					<a href="<?= site_url('about') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_about : $menu->en_about ?></a>
				</li>	
				<li>
					<a href="#"><?= $this->session->userdata('arabic') ? $menu->ar_gallery : $menu->en_gallery ?></a>
					<ul>
						<li>
							<a href="<?= site_url('gallery/hall-photos') ?>"><?= $this->session->userdata('arabic') ? 'قاعة الصور' : 'Hall Photos' ?></a>
						</li>
						<li>
							<a href="<?= site_url('gallery/events-photos') ?>"><?= $this->session->userdata('arabic') ? 'معرض الأحداث' : 'Events Gallery' ?></a>
						</li>
						<li>
							<a href="<?= site_url('gallery/video-gallery') ?>"><?= $this->session->userdata('arabic') ? 'معرض الفيديو' : 'Video Gallery' ?></a>
						</li>
					</ul>
				</li>	
				<li>
					<a href="<?= site_url('services') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_services : $menu->en_services ?></a>
				</li>	
				<li>
					<a href="<?= site_url('events') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_events : $menu->en_events ?></a>
				</li>	
				<li>
					<a href="<?= site_url('contact') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_contact : $menu->en_contact ?></a>
				</li>	
				<li>
					<a href="<?= site_url('reservation') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_reservation : $menu->en_reservation ?></a>
				</li>
				<li><a href="#" class="lang_switch" data-url="<?= current_url() ?>" data-id="<?= $this->session->userdata('arabic') ? 'arabic' : 'english' ?>"><?= $this->session->userdata('arabic') ? 'English' : 'عربى' ?></a></li>
			</ul>
		</nav>
	</div>
</header>