<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>malekat</title>
		<!-- Stylesheets -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="shortcut icon" href="<?= base_url('assets/images/logo.png') ?>" type="image/x-icon">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
		<!-- Responsive -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<link href="<?= base_url('assets/css/responsive.css') ?>" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Montserrat|Lobster|Cairo:400,600&amp;subset=arabic" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.2/css/lightgallery.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/minimal/orange.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.2.0/aos.css">
        <?php if($this->session->userdata('arabic')){ ?>
			<style type="text/css">
				body, .main-menu li a, .head-up, .head-down, div.col-md-3.col-sm-6.nopadding:nth-of-type(even) h3,div.col-md-3.col-sm-6.nopadding:nth-of-type(odd) h3, div.col-md-3.col-sm-6.nopadding:nth-of-type(odd) p, div.col-md-3.col-sm-6.nopadding:nth-of-type(even) p, .about-section h1, .about-section h3, .about-section p, .main-service h3, .arabic_service h4, .event-section h1, .event-section h3, .gallery-section h1, footer .links h4, .hover h3, .heading h1, .heading h2{
					font-family: 'Cairo', sans-serif !important;
					font-style: normal !important;
				}
			</style>
        <?php } ?>
	</head>

	<body>