<div id="mycarousel" class="carousel slide carousel-fade" data-ride="carousel">
	<div class="carousel-inner">
		<?php $i = 0; foreach ($sliders as $slider) { ?>
		<div class="item <?= (!$i) ? 'active' : '' ?>">
			<div style="background-image: url(<?= base_url('uploads/sliders/'.$slider->image) ?>);" class="main-slider">
				<div class="carousel-caption">
					<div class="head-up animated zoomIn"><?= $this->session->userdata('arabic') ? $slider->ar_title : $slider->en_title ?></div>
					<div class="head-down animated bounceIn"><?= $this->session->userdata('arabic') ? $slider->ar_title_large : $slider->en_title_large ?></div>
				</div>
			</div>
		</div>
		<?php $i++; } ?>
	</div>

	<a class="left carousel-control" href="#mycarousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#mycarousel" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
</div>
<div class="container-fluid">
	<div class="row services">
		<div class="container">
			<div class="row">
				<?php foreach ($services as $service) { ?>
				<div class="col-md-3 col-sm-6 nopadding">
					<div class="service text-center <?= $this->session->userdata('arabic') ? 'arabic' : '' ?>" data-aos="fade-up" data-aos-duration="1000">
						<h3><?= $this->session->userdata('arabic') ? $service->ar_name : $service->en_name ?></h3>
						<img src="<?= base_url('uploads/services/'.$service->image) ?>" class="img-responsive img-circle center-block">
						<p><?= $this->session->userdata('arabic') ? (strlen($service->ar_desc) < 200 ? $service->ar_desc : substr($service->ar_desc, 0, 200).'...') : (strlen($service->en_desc) < 200 ? $service->en_desc : substr($service->en_desc, 0, 200).'...') ?></p><br>
						<a href="<?= site_url('services') ?>/#menu"><?= $this->session->userdata('arabic') ? 'اقرأ أكثر' : 'Read More' ?></a>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row about-section">
		<div class="container">
			<div class="row <?= $this->session->userdata('arabic') ? 'arabic_about' : '' ?>">
				<div class="col-sm-12 text-center">
					<h1><?= $this->session->userdata('arabic') ? 'حول ماليكات' : 'About Malekat' ?></h1><br>
				</div>
				<div class="col-sm-7" data-aos="<?= $this->session->userdata('arabic') ? 'fade-left' : 'fade-right' ?>" data-aos-duration="1000">
					<h3><?= $this->session->userdata('arabic') ? $about_us->ar_title : $about_us->en_title ?></h3>
					<p><?= $this->session->userdata('arabic') ? $about_us->ar_desc : $about_us->en_desc ?></p>
					<br>
					<a href="<?= site_url('about') ?>"><?= $this->session->userdata('arabic') ? 'اقرأ أكثر' : 'Read More' ?></a>
				</div>
				<div class="col-sm-5" data-aos="<?= $this->session->userdata('arabic') ? 'fade-right' : 'fade-left' ?>" data-aos-duration="1000">
					<img src="<?= base_url('assets/images/about.jpg') ?>" class="img-responsive">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row main-service <?= $this->session->userdata('arabic') ? 'arabic_service' : '' ?>" data-aos="fade-up" data-aos-duration="1000">
		<?php $i=1; foreach ($highlight_services as $service) { ?>
		<?php if ($i%2 == 0) { ?>
		<div class="col-xs-12 col-md-6 nopadding">
			<div style="background-image: url('<?= base_url('uploads/services/'.$service->image) ?>');background-size: cover;width: 100%;height: 430px;" class="service-bg"></div>
		</div>
		<?php } ?>
		<div class="col-xs-12 col-md-6 pl100 content">
			<h3><?= $this->session->userdata('arabic') ? $service->ar_name : $service->en_name ?></h3>
			<div class="clearfix"></div><br>
			<h4><?= $this->session->userdata('arabic') ? $service->ar_title : $service->en_title ?></h4>
			<p><?= $this->session->userdata('arabic') ? $service->ar_desc : $service->en_desc ?></p>
		</div>
		<?php if ($i%2) { ?>
		<div class="col-xs-12 col-md-6 nopadding">
			<div style="background-image: url('<?= base_url('uploads/services/'.$service->image) ?>');background-size: cover;width: 100%;height: 400px;" class="service-bg"></div>
		</div>
		<?php } ?>
		<?php $i++; } ?>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container-fluid">
	<div class="row event-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h1><?= $this->session->userdata('arabic') ? 'أحدث الأحداث لدينا' : 'Our Latest Events' ?></h1>
					<div class="clearfix"></div><br>
					<h3><?= $this->session->userdata('arabic') ? 'ونحن نستضيف بفخر بعض الأحداث الدولية' : 'We proudly host some international events' ?></h3>
				</div>
				<div class="clearfix"></div><br>	
				<?php foreach ($events as $event) { ?>
				<div class="col-sm-4" data-aos="fade-up" data-aos-duration="1000">
					<a href="<?= site_url('events') ?>" style="cursor: pointer;">
						<div class="hover ehover42">
							<img class="img-responsive" src="<?= base_url('uploads/events/'.$event->image) ?>" alt="">
							<div class="overlay">
								<h2><?= $this->session->userdata('arabic') ? $event->ar_name : $event->en_name ?></h2>
								<p class="info"><?= $this->session->userdata('arabic') ? $event->ar_title : $event->en_title ?></p>
							</div>				
						</div>
					</a>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row gallery-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h1><?= $this->session->userdata('arabic') ? $menu->ar_gallery : $menu->en_gallery ?></h1>
				</div>
				<div class="clearfix"></div><br><br>
				<div id="gallery">
					<?php foreach ($gallery as $value) { ?>
					<a href="<?= base_url('uploads/gallery/'.$value->image) ?>">
						<div class="col-sm-4" data-aos="fade-up" data-aos-duration="1000">
							<div class="hover ehover13">
								<img src="<?= base_url('uploads/gallery/'.$value->image) ?>" class="img-responsive">
								<div class="overlay">
									<h2><i class="fa fa-search"></i></h2>
									<h3><?= $this->session->userdata('arabic') ? $value->ar_title : $value->en_title ?></h3>
								</div>
							</div>
						</div>
					</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>