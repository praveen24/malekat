<div class="container-fluid">
	<div class="row heading">
		<div class="col-sm-12 text-center">
			<h1><?= $this->session->userdata('arabic') ? 'سياسة الخصوصية' : 'Privacy Policy' ?></h1>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row main-service">
		<div class="col-sm-10 col-sm-offset-1">
			<?= $this->session->userdata('arabic') ? $privacy->ar_content : $privacy->en_content ?>
		</div>
	</div>
</div>