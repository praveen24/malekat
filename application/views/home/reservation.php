<div class="container-fluid">
	<div class="row heading">
		<div class="col-sm-12 text-center">
			<h1><?= $this->session->userdata('arabic') ? 'حجز' : 'Reservation' ?></h1>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row contact-section">
		<div class="col-md-12">
			<?php if ($this->session->flashdata('success')) : ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php elseif ($this->session->flashdata('error')) : ?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php elseif ($this->session->flashdata('info')) : ?>
				<div class="alert alert-info alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $this->session->flashdata('info'); ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="col-sm-8 col-sm-offset-2 reservation-box <?= $this->session->userdata('arabic') ? 'arabic_contact' : '' ?>" data-aos="fade-up">
			<div class="col-md-7">
				<form action="reservation_info" method="post">
					<div class="form-group">
						<select class="form-control" name="res_type" dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>">
							<option value=""><?= $this->session->userdata('arabic') ? 'نوع الحجز' : 'Reservation Type' ?></option>
							<option value="Wedding Party"><?= $this->session->userdata('arabic') ? 'حفلة الزفاف' : 'Wedding Party' ?></option>
							<option value="Corporate Event"><?= $this->session->userdata('arabic') ? 'مناسبات الشركات' : 'Corporate Event' ?></option>
							<option value="Other Events"><?= $this->session->userdata('arabic') ? 'مناسبات أخرى' : 'Other Events' ?></option>
						</select>
					</div>
					<div class="form-group">
						<div class="row" style="<?= $this->session->userdata('arabic') ? 'display: flex;flex-direction: row-reverse;flex-wrap: wrap;' : '' ?>">
							<div class="col-sm-4" dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>">
								<input type="checkbox" name="section[]" class="checkbox" value="Morning"> &nbsp;<?= $this->session->userdata('arabic') ? 'صباح' : 'Morning' ?>
							</div>
							<div class="col-sm-4" dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>">
								<input type="checkbox" name="section[]" class="checkbox" value="Evening"> &nbsp;<?= $this->session->userdata('arabic') ? 'مساء' : 'Evening' ?>&emsp;
							</div>
							<div class="col-sm-4" dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>">
								<input type="checkbox" name="section[]" class="checkbox" value="Night"> &nbsp;<?= $this->session->userdata('arabic') ? 'ليل' : 'Night' ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<input type="text" name="name" placeholder="<?= $this->session->userdata('arabic') ? 'الاسم' : 'Name' ?>" class="form-control" required dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>">
					</div>
					<div class="form-group">
						<input type="email" name="email" placeholder="<?= $this->session->userdata('arabic') ? 'البريد الإلكتروني' : 'Email' ?>" class="form-control" required dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>">
					</div>
					<div class="form-group">
						<input type="text" name="phone" placeholder="<?= $this->session->userdata('arabic') ? 'رقم الهاتف' : 'Phone Number' ?>" class="form-control" required dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>">
					</div>
					<div class="form-group">
						<input class="datepicker form-control" name="date" data-date-format="mm/dd/yyyy" placeholder="<?= $this->session->userdata('arabic') ? 'تاريخ الحجز' : 'Reservation Date' ?>" required style="<?= $this->session->userdata('arabic') ? 'direction:rtl' : '' ?>">
					</div>
					<div class="form-group">
						<input type="text" name="exp_no" placeholder="<?= $this->session->userdata('arabic') ? 'العدد المتوقع' : 'Expected number' ?>" class="form-control" required dir="<?= $this->session->userdata('arabic') ? 'rtl' : '' ?>">
					</div>
					<div class="form-group">
						<button class="btn btn-send btn-block" type="submit"><?= $this->session->userdata('arabic') ? 'حجز' : 'SUBMIT' ?></button>
					</div>
				</form>
			</div>
			<div class="col-md-5 logo-image">
				<img src="<?= base_url('assets/images/logo.png') ?>" class="img-responsive center-block">
			</div>
		</div>
	</div>
</div>