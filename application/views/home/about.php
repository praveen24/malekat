<div class="container-fluid">
	<div class="row heading">
		<div class="col-sm-12 text-center">
			<h1><?= $this->session->userdata('arabic') ? $menu->ar_about : $menu->en_about ?></h1>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row about-section">
		<div class="container">
			<div class="row <?= $this->session->userdata('arabic') ? 'arabic_about' : '' ?>">
				<div class="col-sm-7" data-aos="<?= $this->session->userdata('arabic') ? 'fade-left' : 'fade-right' ?>" data-aos-duration="1000">
					<h3><?= $this->session->userdata('arabic') ? $about_us->ar_title : $about_us->en_title ?></h3>
					<p><?= $this->session->userdata('arabic') ? $about_us->ar_desc : $about_us->en_desc ?></p>
				</div>
				<div class="col-sm-5" data-aos="<?= $this->session->userdata('arabic') ? 'fade-right' : 'fade-left' ?>" data-aos-duration="1000">
					<img src="<?= base_url('assets/images/about.jpg') ?>" class="img-responsive">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<hr>
				</div>
			</div>
			<div class="col-sm-12 text-center <?= $this->session->userdata('arabic') ? 'arabic_history' : '' ?>">
				<h1><?= $this->session->userdata('arabic') ? 'تاريخنا' : 'Our History' ?></h1><br>
				<h3><?= $this->session->userdata('arabic') ? 'التاريخ يتطور، وتقف الفن لا يزال قائماx' : 'History develops, art stands still' ?></h3>
				<section class="timeline"> 
  					<ul>
            		<?php foreach ($about_us_history as $value) { ?>
    					<li>
      						<div class="content" data-aos="zoom-in">
        						<h2>
          							<time><?= $this->session->userdata('arabic') ? $value->ar_year : $value->en_year ?></time>
        						</h2>
        						<h3><?= $this->session->userdata('arabic') ? $value->ar_title : $value->en_title ?></h3>
        						<p><?= $this->session->userdata('arabic') ? $value->ar_desc : $value->en_desc ?></p>
      						</div>
    					</li>
              		<?php } ?>
  					</ul>
				</section>
			</div>
		</div>
	</div>
</div>