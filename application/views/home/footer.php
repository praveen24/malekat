		<footer>
			<div class="container">
				<div class="row <?= $this->session->userdata('arabic') ? 'arabic_footer' : '' ?>">
					<div class="col-sm-4">
						<img src="<?= base_url('assets/images/logo.png') ?>" class="img-responsive center-block">
						<p><?= $this->session->userdata('arabic') ? substr($about_us->ar_desc, 0, 400) : substr($about_us->en_desc, 0, 400) ?></p>
					</div>
					<div class="col-sm-4 links">
						<h4><?= $this->session->userdata('arabic') ? 'روابط سريعة' : 'QUICK LINKS' ?></h4>
						<div class="row" style="<?= $this->session->userdata('arabic') ? 'display: flex;flex-direction: row-reverse;flex-wrap: wrap;' : '' ?>">
							<div class="col-sm-6">
								<ul style="<?= $this->session->userdata('arabic') ? 'direction: rtl' : '' ?>">
									<li><a href="<?= site_url() ?>"><?= $this->session->userdata('arabic') ? $menu->ar_home : $menu->en_home ?></a></li>
									<li><a href="<?= site_url('about') ?>"><?= $this->session->userdata('arabic') ? $menu->ar_about : $menu->en_about ?></a></li>
									<li><a href="<?= site_url('privacy_policy') ?>"><?= $this->session->userdata('arabic') ? 'سياسة الخصوصية' : 'Privacy Policy' ?></a></li>
								</ul>
							</div>
							<div class="col-sm-6">
								<ul style="<?= $this->session->userdata('arabic') ? 'direction: rtl' : '' ?>">
									<li><a href="<?= site_url('services') ?>"><?= $this->session->userdata('arabic') ? 'خدمات' : 'Services' ?></a></li>
									<li><a href="<?= site_url('contact') ?>"><?= $this->session->userdata('arabic') ? 'اتصل بنا' : 'Contact Us' ?></a></li>
									<li><a href="<?= site_url('reservation') ?>"><?= $this->session->userdata('arabic') ? 'حجز' : 'Reservation' ?></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-4 links">
						<h4><?= $this->session->userdata('arabic') ? 'أبق على اتصال' : 'KEEP IN TOUCH' ?></h4>
						<p><i class="fa fa-map-marker" style="<?= $this->session->userdata('arabic') ? 'margin-left: 10px;' : 'margin-right: 10px;float: right: ' ?>"></i><?= $this->session->userdata('arabic') ? '٧٨٠٨ عبد الله آل مانياي، أبهور<br>حي الجابرية، جدة<br>١٣٧١٩، المملكة العربية السعودية' : '7808 Abdullah Al Maniaai, <br>Abhur Al Janubiyah District, <br>Jeddah 23719, Saudi Arabia' ?></p>
						<p style="<?= $this->session->userdata('arabic') ? 'direction: initial;float: right;flex-direction: row-reverse;flex-wrap: wrap;' : '' ?>"><i class="fa fa-phone" style="<?= $this->session->userdata('arabic') ? 'margin-left: 10px;' : 'margin-right: 10px;' ?>"></i><?= $this->session->userdata('arabic') ? '+٩٦٦١٢٦٩٩٢٠٢٠' : '+966 12 699 2020' ?></p>
						<p><i class="fa fa-envelope" style="<?= $this->session->userdata('arabic') ? 'margin-left: 10px;' : 'margin-right: 10px;' ?>"></i>info@malekat.com</p>
						<div class="header-top">
							<ul>
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-snapchat-ghost"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-youtube-play"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="br"></div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row copyright">
					<div class="col-sm-12">
						<p class="text-center">&copy; <a href="#">Malekat</a>. All Rights Reserved</p>
					</div>
				</div>
			</div>
		</footer>
		<script type="text/javascript">
			var site_url = '<?php echo site_url(); ?>';
		</script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.2/js/lightgallery-all.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.2.0/aos.js"></script>
		<script type="text/javascript" src="<?= base_url('assets/js/script.js') ?>"></script>
	</body>
</html>