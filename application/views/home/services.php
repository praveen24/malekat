<div class="container-fluid">
	<div class="row heading">
		<div class="col-sm-12 text-center">
			<h1><?= $this->session->userdata('arabic') ? 'خدمات' : 'Services' ?></h1>
			<h2><?= $this->session->userdata('arabic') ? 'مساحات فاخرة للأحداث لا تنسى' : 'Luxurious Spaces for Memorable events' ?></h2>
		</div>
	</div>
</div>
<div class="container-fluid">
<?php $i=1; foreach ($services as $service) { ?>
	<div class="row main-service <?= $this->session->userdata('arabic') ? 'arabic_service' : '' ?>" data-aos="fade-up">
		<?php if ($i%2 == 0) { ?>
		<div class="col-xs-12 col-sm-6 nopadding">
			<div style="background-image: url('<?= base_url('uploads/services/'.$service->image) ?>');background-size: cover;width: 100%;height: 400px;" class="service-bg"></div>
		</div>
		<?php } ?>
		<div class="col-xs-12 col-sm-6 pl100 content">
			<h3><?= $this->session->userdata('arabic') ? $service->ar_name : $service->en_name ?></h3>
			<div class="clearfix"></div><br>
			<h4><?= $this->session->userdata('arabic') ? $service->ar_title : $service->en_title ?></h4>
			<p><?= $this->session->userdata('arabic') ? $service->ar_desc : $service->en_desc ?></p>
		</div>
		<?php if ($i%2) { ?>
		<div class="col-xs-12 col-sm-6 nopadding">
			<div style="background-image: url('<?= base_url('uploads/services/'.$service->image) ?>');background-size: cover;width: 100%;height: 400px;" class="service-bg"></div>
		</div>
		<?php } ?>
	</div>
	<?php $i++; } ?>
</div>
<div class="container-fluid">
	<div class="row event-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h1><?= $this->session->userdata('arabic') ? 'أحدث الأحداث لدينا' : 'Our Latest Events' ?></h1></h1>
					<div class="clearfix"></div><br>
					<h3><?= $this->session->userdata('arabic') ? 'ونحن نستضيف بفخر بعض الأحداث الدولية' : 'We proudly host some international events' ?></h3>
				</div>
				<div class="clearfix"></div><br>	
				<?php foreach ($events as $event) { ?>
				<div class="col-sm-4">
					<div class="hover ehover42">
						<img class="img-responsive" src="<?= base_url('uploads/events/'.$event->image) ?>" alt="">
						<div class="overlay">
							<h2><?= $this->session->userdata('arabic') ? $event->ar_name : $event->en_name ?></h2>
							<p class="info"><?= $this->session->userdata('arabic') ? $event->ar_title : $event->en_title ?></p>
						</div>				
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>