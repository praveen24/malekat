<div class="container-fluid">
	<div class="row heading">
		<div class="col-sm-12 text-center">
			<h1><?= $this->session->userdata('arabic') ? 'معرض الفيديو' : 'Video Gallery' ?></h1>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row gallery-section">
		<div class="container">
			<div class="row">
				<div id="gallery">
				<?php foreach ($gallery as $value) {
					$link = $value->link;
					$video_id = explode("?v=", $link);
					$video_id = $video_id[1];
					$img = 'https://img.youtube.com/vi/'.$video_id.'/0.jpg'; ?>
					<a href="<?= $value->link ?>">
						<div class="col-sm-4" data-aos="fade-up">
							<div class="hover ehover13">
								<img src="<?= $img ?>" class="img-responsive">
								<div class="overlay">
									<h2><i class="fa fa-youtube-play" style="font-size: 25px"></i></h2>
									<h3>Malekat</h3>
								</div>
							</div>
						</div>
					</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>