<aside>
    <div id="sidebar" class="nav-collapse">
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="active" href="<?= site_url('admin/home') ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/sliders') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>Sliders </span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-book"></i>
                        <span>Gallery</span>
                    </a>
                    <ul class="sub">
                    <li><a href="<?= site_url('admin/gallery') ?>">Hall Photos</a></li>
                    <li><a href="<?= site_url('admin/event_gallery') ?>">Event Gallery</a></li>
                    <li><a href="<?= site_url('admin/video_gallery') ?>">Video Gallery</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?= site_url('admin/services') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>Services </span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/events') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>Events </span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/menu') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>Menu </span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/contact_list') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span> Contact List </span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/reservation_list') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span> Reservation List </span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/about_us') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>About Us </span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/about_us_history') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>About Us History </span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('admin/privacy') ?>">
                        <i class="fa fa-bullhorn"></i>
                        <span>Privacy Policy </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</aside>