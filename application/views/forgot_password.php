<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vineeth N Krishnan">
    <meta name="description" content="Page description. No longer than 155 characters." />
    <meta itemprop="name" content="The Name or Title Here">
    <meta itemprop="description" content="This is the page description">
    <meta itemprop="image" content="http://www.example.com/image.jpg">
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@way2vineeth">
    <meta name="twitter:title" content="Page Title">
    <meta name="twitter:description" content="Page description less than 200 characters">
    <meta name="twitter:creator" content="@way2vineeth">
    <meta name="twitter:image" content="http://www.example.com/image.jpg">
    <meta name="twitter:data1" content="">
    <meta name="twitter:label1" content="Price">
    <meta name="twitter:data2" content="Black">
    <meta name="twitter:label2" content="Color">
    <meta property="og:title" content="Title Here" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://www.example.com/" />
    <meta property="og:image" content="http://example.com/image.jpg" />
    <meta property="og:description" content="Description Here" />
    <meta property="og:site_name" content="Site Name, i.e. Moz" />
    <meta property="og:price:amount" content="15.00" />
    <meta property="og:price:currency" content="USD" />
    <title>News Portal</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/sweetalert.css') ?>">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
</head><body>
<div class="row">
    <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3" style="position: relative;margin-top: 10%;">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Forgot Password</h3>
            </div>
            <div class="panel-body">
                <p class="login-box-msg"></p>
                <!-- <?php if ($message != ''): ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?> -->
                <form novalidate="novalidate" id="da-login-form" method="post" action="<?php echo site_url('auth/forgot_password'); ?>">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Username/Email" name="identity" value="<?php echo set_value('identity'); ?>"/>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-5">    
                            <button type="button" onclick="window.location.href = '<?= site_url('auth/login') ?>'" class="btn btn-default btn-block btn-flat"> Cancel </button>
                            </div><!-- /.col -->
                        <div class="col-xs-5">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                        </div><!-- /.col -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</body>

