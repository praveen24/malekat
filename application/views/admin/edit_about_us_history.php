<section id="main-content">
  <section class="wrapper">

    <?php $this->load->view('layouts/notification'); ?>
    <div class="panel  panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title"> Edit About Us History
                <div class="clearfix">  </div>
            </h2>
        </div>
        <!-- /.box-header -->

        <div class="panel-body">
            <form method="POST" action="<?= site_url('admin/edit_about_us_history/'.$about_us_history->id) ?>" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-5 col-md-offset-2 text-center">
                        <label>English</label>
                    </div>
                    <div class="col-md-5 text-center">
                        <label>Arabic</label>
                    </div>
                </div>
                <div class="clearfix"></div><br>

                <div class="row">
                    <div class="col-md-2">
                        <label> Year </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_year') ? 'has-error' : '' ?>">
                        <input type="text" name="en_year" id="en_year" class="form-control" placeholder="Enter the English Year here" value="<?= $about_us_history->en_year ?>">
                        <span class="<?= form_error('en_year') ? 'text-danger' : '' ?>"><?= form_error('en_year') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_year') ? 'has-error' : '' ?>">
                        <input type="text" dir="rtl" name="ar_year" id="ar_year" class="form-control" placeholder="Enter the Arabic Year here" value="<?= $about_us_history->ar_year ?>">
                        <span class="<?= form_error('ar_year') ? 'text-danger' : '' ?>"><?= form_error('ar_year') ?></span>
                    </div>
                </div>
                <div class="clearfix"></div><br>

                <div class="row">
                    <div class="col-md-2">
                        <label> Title </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_title') ? 'has-error' : '' ?>">
                        <input type="text" name="en_title" id="en_title" class="form-control" placeholder="Enter the English Title here" value="<?= $about_us_history->en_title ?>">
                        <span class="<?= form_error('en_title') ? 'text-danger' : '' ?>"><?= form_error('en_title') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_title') ? 'has-error' : '' ?>">
                        <input type="text" dir="rtl" name="ar_title" id="ar_title" class="form-control" placeholder="Enter the Arabic Title here" value="<?= $about_us_history->ar_title ?>">
                        <span class="<?= form_error('ar_title') ? 'text-danger' : '' ?>"><?= form_error('ar_title') ?></span>
                    </div>
                </div>
                <div class="clearfix"></div><br>

                <div class="row">
                    <div class="col-md-2">
                        <label> Description </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_desc') ? 'has-error' : '' ?>">
                        <textarea name="en_desc" id="en_desc" class="form-control" placeholder="Enter the English Description here" rows="5"><?= $about_us_history->en_desc ?></textarea>
                        <span class="<?= form_error('en_desc') ? 'text-danger' : '' ?>"><?= form_error('en_desc') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_desc') ? 'has-error' : '' ?>">
                        <textarea name="ar_desc" id="ar_desc" class="form-control" placeholder="Enter the Arabic Description here" rows="5" dir="rtl"><?= $about_us_history->ar_desc ?></textarea>
                        <span class="<?= form_error('ar_desc') ? 'text-danger' : '' ?>"><?= form_error('ar_desc') ?></span>
                    </div>
                </div>
                <div class="clearfix"></div><br>
                <button type="submit" class="btn btn-primary" id="submit2" >Submit</button>
                <button type="button" class="btn btn-default" onclick="history.go(-1);">Back</button>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</section>
</section>

<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript">

    var base_url = "<?= base_url() ?>";

    $(function () {
        CKEDITOR.replace('en_desc');
        CKEDITOR.replace('ar_desc', {
            contentsLangDirection: 'rtl'
        });
    });

</script>