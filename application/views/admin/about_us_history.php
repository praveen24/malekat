<section id="main-content">
  <section class="wrapper">

    <?php $this->load->view('layouts/notification'); ?>
    <div class="panel  panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title">About Us History
                <a href="<?= site_url('admin/add_about_us_history') ?>" class="btn btn-sm btn-success pull-right">Add About Us History</a>
                <div class="clearfix">  </div>
            </h2>

        </div>
        <!-- /.box-header -->

        <div class="panel-body">
            <div class="box box-primary">
                <form name="searchform" method="get" action="">
                    <div class="box-body">
                        <div class="input-group">
                            <div class="" style="padding-left: 0;">
                                <input type="text" name="content" placeholder="Enter the search key" class="form-control" value="<?= $this->input->get('content') ?>"/>
                            </div>
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary" name="search" value="true">Search</button>
                                <a class="btn btn-danger class_for_clear" >Clear</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- /.box -->
            <br />
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Year</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($about_us_history) {
                        foreach ($about_us_history as $key => $value) {
                            ?>
                            <tr>
                                <td><?= (($page - 1) * $perpage + ($key + 1)) ?></td>
                                <td><?= $value->en_year ?></td>
                                <td><?= $value->en_title ?></td>
                                <td><?= substr($value->en_desc, 0, 50).'...' ?></td>
                                <td style="width:10%;">
                                    <a href="<?= site_url('admin/delete_about_us_history/' . $value->id) ?>" onclick="return confirm('Are you sure you want to delete the History ?');" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                    <a href="<?= site_url('admin/edit_about_us_history/' . $value->id) ?>" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="12">
                                <div class="alert alert-danger text-center">
                                    <strong><i class="fa fa-exclamation-triangle"></i> No Results found </strong>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?= isset($pagination) && $pagination ? $pagination : '' ?>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</section>
</section>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $('.class_for_clear').click(function () {
            $('input[name="content"]').val('');
        });
    });
</script>