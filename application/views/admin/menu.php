<section id="main-content">
  <section class="wrapper">

    <?php $this->load->view('layouts/notification'); ?>
    <div class="panel  panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title"> Manage Menu
                <div class="clearfix">  </div>
            </h2>
        </div>
        <!-- /.box-header -->

        <div class="panel-body">
            <form method="POST" action="<?= site_url('admin/menu') ?>" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-5 col-md-offset-2 text-center">
                        <label>English</label>
                    </div>
                    <div class="col-md-5 text-center">
                        <label>Arabic</label>
                    </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="row">
                    <div class="col-md-2">
                        <label> Home </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_home') ? 'has-error' : '' ?>">
                        <input type="text" name="en_home" id="en_home" class="form-control" placeholder="Enter the English Home here" value="<?= $menu->en_home ?>">
                        <span class="<?= form_error('en_home') ? 'text-danger' : '' ?>"><?= form_error('en_home') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_home') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_home" dir="rtl" id="ar_home" class="form-control" placeholder="Enter the Arabic Home here" value="<?= $menu->ar_home ?>">
                        <span class="<?= form_error('ar_home') ? 'text-danger' : '' ?>"><?= form_error('ar_home') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> About </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_about') ? 'has-error' : '' ?>">
                        <input type="text" name="en_about" id="en_about" class="form-control" placeholder="Enter the English -About here" value="<?= $menu->en_about ?>">
                        <span class="<?= form_error('en_about') ? 'text-danger' : '' ?>"><?= form_error('en_about') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_about') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_about" dir="rtl" id="ar_about" class="form-control" placeholder="Enter the Arabic -About here" value="<?= $menu->ar_about ?>">
                        <span class="<?= form_error('ar_about') ? 'text-danger' : '' ?>"><?= form_error('ar_about') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Reservation </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_reservation') ? 'has-error' : '' ?>">
                        <input type="text" name="en_reservation" id="en_reservation" class="form-control" placeholder="Enter the English - Reservation here" value="<?= $menu->en_reservation ?>">
                        <span class="<?= form_error('en_reservation') ? 'text-danger' : '' ?>"><?= form_error('en_reservation') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_reservation') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_reservation" dir="rtl" id="ar_reservation" class="form-control" placeholder="Enter the Arabic - Reservation here" value="<?= $menu->ar_reservation ?>">
                        <span class="<?= form_error('ar_reservation') ? 'text-danger' : '' ?>"><?= form_error('ar_reservation') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Services </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_services') ? 'has-error' : '' ?>">
                        <input type="text" name="en_services" id="en_services" class="form-control" placeholder="Enter the English - Services here" value="<?= $menu->en_services ?>">
                        <span class="<?= form_error('en_services') ? 'text-danger' : '' ?>"><?= form_error('en_services') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_services') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_services" dir="rtl" id="ar_services" class="form-control" placeholder="Enter the Arabic - Services here" value="<?= $menu->ar_services ?>">
                        <span class="<?= form_error('ar_services') ? 'text-danger' : '' ?>"><?= form_error('ar_services') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Events </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_events') ? 'has-error' : '' ?>">
                        <input type="text" name="en_events" id="en_events" class="form-control" placeholder="Enter the English - Events here" value="<?= $menu->en_events ?>">
                        <span class="<?= form_error('en_events') ? 'text-danger' : '' ?>"><?= form_error('en_events') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_events') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_events" dir="rtl" id="ar_events" class="form-control" placeholder="Enter the Arabic - Events here" value="<?= $menu->ar_events ?>">
                        <span class="<?= form_error('ar_events') ? 'text-danger' : '' ?>"><?= form_error('ar_events') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Gallery </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_gallery') ? 'has-error' : '' ?>">
                        <input type="text" name="en_gallery" id="en_gallery" class="form-control" placeholder="Enter the English -Gallery here" value="<?= $menu->en_gallery ?>">
                        <span class="<?= form_error('en_gallery') ? 'text-danger' : '' ?>"><?= form_error('en_gallery') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_gallery') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_gallery" dir="rtl" id="ar_gallery" class="form-control" placeholder="Enter the Arabic -Gallery here" value="<?= $menu->ar_gallery ?>">
                        <span class="<?= form_error('ar_gallery') ? 'text-danger' : '' ?>"><?= form_error('ar_gallery') ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label> Contact </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_contact') ? 'has-error' : '' ?>">
                        <input type="text" name="en_contact" id="en_contact" class="form-control" placeholder="Enter the English -Contact here" value="<?= $menu->en_contact ?>">
                        <span class="<?= form_error('en_contact') ? 'text-danger' : '' ?>"><?= form_error('en_contact') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_contact') ? 'has-error' : '' ?>">
                        <input type="text" name="ar_contact" dir="rtl" id="ar_contact" class="form-control" placeholder="Enter the Arabic -Contact here" value="<?= $menu->ar_contact ?>">
                        <span class="<?= form_error('ar_contact') ? 'text-danger' : '' ?>"><?= form_error('ar_contact') ?></span>
                    </div>
                </div>
                <div class="clearfix"></div><br>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-default" onclick="history.go(-1);">Back</button>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</section>
</section>