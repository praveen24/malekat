<section id="main-content">
  <section class="wrapper">

    <?php $this->load->view('layouts/notification'); ?>
    <div class="panel  panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title"> Edit Event
                <div class="clearfix">  </div>
            </h2>
        </div>
        <!-- /.box-header -->

        <div class="panel-body">
        <form method="POST" action="<?= site_url('admin/edit_event/'.$event->id) ?>" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-5 col-md-offset-2 text-center">
                        <label>English</label>
                    </div>
                    <div class="col-md-5 text-center">
                        <label>Arabic</label>
                    </div>
                </div>
                <div class="clearfix"></div><br>

                <div class="row">
                    <div class="col-md-2">
                        <label> Name </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_name') ? 'has-error' : '' ?>">
                    <input type="text" name="en_name" id="en_name" class="form-control" placeholder="Enter the English Name here" value="<?= $event->en_name ?>">
                        <span class="<?= form_error('en_name') ? 'text-danger' : '' ?>"><?= form_error('en_name') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_name') ? 'has-error' : '' ?>">
                    <input type="text" dir="rtl" name="ar_name" id="ar_name" class="form-control" placeholder="Enter the Arabic Name here" value="<?= $event->ar_name ?>">
                        <span class="<?= form_error('ar_name') ? 'text-danger' : '' ?>"><?= form_error('ar_name') ?></span>
                    </div>
                </div>
                <div class="clearfix"></div><br>

                <div class="row">
                    <div class="col-md-2">
                        <label> Title </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_title') ? 'has-error' : '' ?>">
                    <input type="text" name="en_title" id="en_title" class="form-control" placeholder="Enter the English Title here" value="<?= $event->en_title ?>">
                        <span class="<?= form_error('en_title') ? 'text-danger' : '' ?>"><?= form_error('en_title') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_title') ? 'has-error' : '' ?>">
                    <input type="text" dir="rtl" name="ar_title" id="ar_title" class="form-control" placeholder="Enter the Arabic Title here" value="<?= $event->ar_title ?>">
                        <span class="<?= form_error('ar_title') ? 'text-danger' : '' ?>"><?= form_error('ar_title') ?></span>
                    </div>
                </div>
                <div class="clearfix"></div><br>

                <div class="row">
                    <div class="col-md-2">
                        <label> Description </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_desc') ? 'has-error' : '' ?>">
                    <textarea name="en_desc" id="en_desc" class="form-control" placeholder="Enter the English Description here" rows="5"><?= $event->en_desc ?></textarea>
                        <span class="<?= form_error('en_desc') ? 'text-danger' : '' ?>"><?= form_error('en_desc') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_desc') ? 'has-error' : '' ?>">
                    <textarea name="ar_desc" id="ar_desc" class="form-control" placeholder="Enter the Arabic Description here" rows="5" dir="rtl"><?= $event->ar_desc ?></textarea>
                        <span class="<?= form_error('ar_desc') ? 'text-danger' : '' ?>"><?= form_error('ar_desc') ?></span>
                    </div>
                </div>
                <div class="clearfix"></div><br>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="exampleInputFile">Image</label>
                    <div class="col-sm-10">
                        <img src="<?= base_url('uploads/events/' . $event->image) ?>" id="prevImage" style="height: 90px; width: 90px;">
                    </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="exampleInputFile">Change Image (preferred size 350*270)</label>
                    <div class="col-sm-10 <?= form_error('uploadImage') ? 'has-error' : '' ?>">
                        <input type="file" name="uploadfile">
                    </div>
                </div>
                <div class="clearfix"></div><br>

                <button type="submit" class="btn btn-primary" id="submit2" >Submit</button>
                <button type="button" class="btn btn-default" onclick="history.go(-1);">Back</button>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</section>
</section>

<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript">

    var base_url = "<?= base_url() ?>";

    $(function () {
        CKEDITOR.replace('en_desc');
        CKEDITOR.replace('ar_desc', {
            contentsLangDirection: 'rtl'
        });
    });

</script>