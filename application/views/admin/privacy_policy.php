<section id="main-content">
  <section class="wrapper">

    <?php $this->load->view('layouts/notification'); ?>
    <div class="panel  panel-primary">
        <div class="panel-heading">
        <h2 class="panel-title"> Manage Privacy Policy
                <div class="clearfix">  </div>
            </h2>
        </div>
        <!-- /.box-header -->

        <div class="panel-body">
            <form method="POST" action="" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-5 col-md-offset-2 text-center">
                        <label>English</label>
                    </div>
                    <div class="col-md-5 text-center">
                        <label>Arabic</label>
                    </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="row">
                    <div class="col-md-2">
                        <label> Content </label>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('en_content') ? 'has-error' : '' ?>">
                    <textarea name="en_content" id="en_content" class="form-control" placeholder="Enter the English Content here" rows="5"><?= $privacy->en_content ?></textarea>
                        <span class="<?= form_error('en_content') ? 'text-danger' : '' ?>"><?= form_error('en_content') ?></span>
                    </div>
                    <div class="col-md-5 form-group <?= form_error('ar_content') ? 'has-error' : '' ?>">
                        <textarea name="ar_content" id="ar_content" class="form-control" placeholder="Enter the Arabic Content here" rows="5" dir="rtl"><?= $privacy->ar_content ?></textarea>
                        <span class="<?= form_error('ar_content') ? 'text-danger' : '' ?>"><?= form_error('ar_content') ?></span>
                    </div>
                </div>
                <div class="clearfix"></div><br>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-default" onclick="history.go(-1);">Back</button>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</section>
</section>

<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {

        CKEDITOR.replace('en_content');
        CKEDITOR.replace('ar_content', {
            contentsLangDirection: 'rtl'
        });

    });
    </script>