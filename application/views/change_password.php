<section id="main-content">
  <section class="wrapper">
    <div class="col-md-9 content">
        <?php $this->load->view('layouts/notification'); ?>
        <div class="panel  panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title"> Change Password
                    <div class="clearfix">  </div>
                </h2>
            </div>
            <div class="panel-body">
                <form novalidate="novalidate" id="da-login-form" method="post" action="<?php echo site_url('admin/change_password'); ?>">
                    <div class="form-group has-feedback col-md-10 col-md-offset-1 <?php echo (form_error('current_password') || isset($current_password_error)) ? 'text-danger' : ''; ?>">
                        <input name="current_password" placeholder="Current Password" type="password" class="form-control" value="<?= set_value('current_password') ?>" />
                        <?php
                        if (isset($passworderror) && $passworderror) {
                            ?>
                            <span class="glyphicon glyphicon-lock form-control-feedback <?php echo ($passworderror || isset($current_password_error)) ? 'text-danger' : ''; ?>"></span>
                            <span class="field_error">Invalid current password</span>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="form-group has-feedback col-md-10 col-md-offset-1 <?php echo (form_error('password') || isset($password_error)) ? 'text-danger' : ''; ?>">
                        <input name="password" placeholder="New Password" type="password" class="form-control"/>
                        <?php
                        if (form_error('password')) {
                            ?>
                            <span class="glyphicon glyphicon-lock form-control-feedback <?php echo (form_error('password') || isset($password_error)) ? 'text-danger' : ''; ?>"></span>
                            <span class="field_error"><?php echo form_error('password'); ?></span>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="form-group has-feedback col-md-10 col-md-offset-1 <?php echo (form_error('confirm_password') || isset($confirm_password_error)) ? 'text-danger' : ''; ?>">
                        <input name="confirm_password" type="password" class="form-control" placeholder="Confirm Password"/>
                        <?php
                        if (form_error('confirm_password')) {
                            ?>
                            <span class="glyphicon glyphicon-lock form-control-feedback <?php echo (form_error('confirm_password') || isset($confirm_password_error)) ? 'text-danger' : ''; ?>"></span>
                            <span class="field_error"><?php echo form_error('confirm_password'); ?></span>
                            <?php
                        }
                        ?>
                    </div>
                    <input name="check" type="hidden"/>
                    <div class="row">
                        <div class="col-md-11 text-center">
                            <button type="submit" class="btn btn-info" style="margin-right:13px;"><i class="glyphicon glyphicon-edit"></i> Submit</button>
                        </div><!-- /.col -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><div class="clearfix"></div><br>
</section>
</section>
