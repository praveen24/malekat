<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	
	public function index()
	{

		$this->data['sliders'] = $this->home_model->getAll('sliders');
		$this->db->limit(4);
		$this->data['services'] = $this->home_model->getWhere(array('status' => 1), 'services');
		$this->db->limit(2);
		$this->data['highlight_services'] = $this->home_model->getWhere(array('highlight' => 1), 'services');
		$this->db->limit(3);
		$this->data['events'] = $this->home_model->getWhere(array('status' => 1), 'events');
		$about = $this->home_model->getAll('about_us');
		$this->data['about_us'] = $about[0];
		$this->db->limit(3);
		$this->data['gallery'] = $this->home_model->getAll('gallery');
                // load views
		$this->data['view_page'] = 'home/index';
		$this->load->view('home/template', $this->data);
	}

	public function about()
	{
        $this->data['about_us_history'] = $this->home_model->getAll('about_us_history');
                // load views
		$this->data['view_page'] = 'home/about';
		$this->load->view('home/template', $this->data);
	}

	public function services()
	{
		$this->db->limit(3);
		$this->data['events'] = $this->home_model->getWhere(array('status' => 1), 'events');
		$this->data['services'] = $this->home_model->getWhere(array('status' => 1), 'services');

                // load views
		$this->data['view_page'] = 'home/services';
		$this->load->view('home/template', $this->data);
	}

	public function events()
	{
		$this->data['events'] = $this->home_model->getWhere(array('status' => 1), 'events');

                // load views
		$this->data['view_page'] = 'home/events';
		$this->load->view('home/template', $this->data);
	}

	public function contact()
	{
                // load views
		$this->data['view_page'] = 'home/contact';
		$this->load->view('home/template', $this->data);
	}

	public function privacy_policy()
	{
		$privacy = $this->home_model->getAll('privacy');
        $this->data['privacy'] = $privacy[0];
                // load views
		$this->data['view_page'] = 'home/privacy';
		$this->load->view('home/template', $this->data);
	}

	public function contact_info(){
		if ($this->input->post()) {
			$data = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'message' => $this->input->post('message'),
				'date_added' => date('Y-m-d h:i'),
				);
			$this->home_model->insertRow($data, 'contact');
			$this->session->set_flashdata('success', 'Thank you. Your Details has been sent. We will contact you soon');
			redirect(site_url('contact'));
		}
	}

	public function manage_language(){
		if ($this->input->post('language') == 'english') {
			$this->session->set_userdata('arabic', 1);
		}
		else{
			$this->session->unset_userdata('arabic');
		}
		echo "true";
	}
	public function reservation()
	{
        // load views
		$this->data['view_page'] = 'home/reservation';
		$this->load->view('home/template', $this->data);
	}

	public function reservation_info(){
		if ($this->input->post()) {
			$sec = implode(',', $this->input->post('section'));
			$data = array(
				'res_type' => $this->input->post('res_type'),
				'section' => $sec,
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'exp_no' => $this->input->post('exp_no'),
				'date' => date('Y-m-d', strtotime($this->input->post('date'))),
				'date_added' => date('Y-m-d h:i'),
				);
			$this->home_model->insertRow($data, 'reservation');
			$this->session->set_flashdata('success', 'Thank you. Your Reservation Details has been sent. We will contact you soon');
			redirect(site_url('reservation'));
		}
	}
	public function hall_photos()
	{
		$this->data['gallery'] = $this->home_model->getAll('gallery');
        // load views
		$this->data['view_page'] = 'home/hall-gallery';
		$this->load->view('home/template', $this->data);
	}
	public function events_gallery()
	{
		$this->data['gallery'] = $this->home_model->getAll('event_gallery');
        // load views
		$this->data['view_page'] = 'home/events-gallery';
		$this->load->view('home/template', $this->data);
	}
	public function video_gallery()
	{
		$this->data['gallery'] = $this->home_model->getAll('video_gallery');
        // load views
		$this->data['view_page'] = 'home/video-gallery';
		$this->load->view('home/template', $this->data);
	}

}