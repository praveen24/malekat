<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('auth_model');
        if (!$this->gr_auth->logged_in() && $this->uri->segment(2) != 'login' && $this->uri->segment(2) != 'logout') {
            if ($this->session->userdata('is_admin')) {
                redirect(site_url('admin/home'), 'refresh');
            }
        }
    }

    public function index() {
        redirect('auth/login', 'refresh');
    }

    public function login() {
        if ($this->gr_auth->logged_in())
            redirect(site_url('admin/home'));
        $this->data['message'] = '';
        $this->data['error'] = '';
        if ($this->input->post()) {
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == true) {
                $remember = (bool) $this->input->post('remember');
                if ($this->gr_auth->login($this->input->post('username'), $this->input->post('password'), $remember)) {
                    if ($this->session->userdata('is_admin')) {
                        redirect(site_url('admin/home'), 'refresh');
                    }
                } else {
                    $this->data['message'] = $this->gr_auth->error();
                }
            } else {
                $this->data['message'] = validation_errors();
            }
        }
        $this->load->view('login', $this->data);
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('auth/login');
    }

    public function forgot_password(){
        $this->load->helper('string');
        if ($this->gr_auth->logged_in())
            redirect(site_url('admin/home'));
        if ($this->input->post('identity')) {
            $user = $this->auth_model->getOneWhere(array('is_admin' => 1), 'user');
            if ($this->input->post('identity') == $user->email) {
             $act_code = random_string('alnum', 15);
             $this->auth_model->updateWhere(array('is_admin' => 1), array('activation_code' => $act_code), 'user');
             // $config = array(
             //    'mailtype' => 'html',
             //    'protocol' => 'smtp',
             //    'smtp_host' => 'ssl://smtp.gmail.com',
             //    'smtp_port' => '465',
             //    'smtp_timeout' => '7',
             //    'smtp_user' => 'fasilk008@gmail.com',
             //    'smtp_pass' => 'gjrsqxauohnslzag',
             //    'charset' => 'utf-8',
             //    'newline' => "\r\n",
             //    'mailtype' => 'html', // or html
             //    'validation' => TRUE  // bool whether to validate email or not
             //    );
             $config = Array(
            'protocol' => 'mail',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'wordwrap' => TRUE,
            'validation' => TRUE
            );

             $this->load->library('email', $config);


             $this->email->from('fasilk008@gmail.com');
             $this->email->to($user->email);

             $this->email->subject('Reset Password');
             $this->email->message('<a href="'.site_url('auth/reset_password/'.$act_code).'">click here for password reset</a>');
             if ($this->email->send()) {
                $this->session->set_flashdata('success', '<span style"color:#fff">Change password link Sent to your Email</span>');
                redirect(site_url('auth/login'));
             }else{
                echo "Error...! Mail cannot be sent";
             }
         }else{
            echo "Error...! Username Mismatch";
        }
    }
    else
        $this->load->view('forgot_password');
}

public function reset_password($act_code){
    $this->data['act_code'] = $act_code;
    $user = $this->auth_model->getOneWhere(array('activation_code' => $act_code), 'user');
    if ($user) {
        if ($this->input->post()) {
            $this->form_validation->set_rules('password', 'New Password', 'trim|required');
            $this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim|required|matches[password]');
            if ($this->form_validation->run($this) == true) {
                $this->auth_model->updateWhere(array('activation_code' => $act_code), array('password' => password_hash($this->input->post('password'), 1), 'activation_code' => ''), 'user');
                $this->session->set_flashdata('success', 'Password Reset Successfully');
                redirect(site_url('auth/login'));
            }else{
                debug(validation_errors());
            }
        }else{
            $this->load->view('reset_password', $this->data);
        }
    }else{
        echo "error..";
    }
}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */