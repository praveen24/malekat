<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends AdminController {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->library('upload');
    }

    public function index() {
        redirect(site_url('auth/login'));
    }

    public function home() {
        $data['view_page'] = 'index.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function change_password() {
        $this->data['user'] = $this->admin_model->getOneWhere(array('id' => $this->session->userdata('user_id')), 'user');
        if ($this->input->post()) {
            if (password_verify($this->input->post('current_password'), $this->data['user']->password)) {
                $this->checkValidationChangePass();
                $this->data['passworderror'] = false;
            } else {
                $this->data['passworderror'] = true;
            }
        }
        $data['view_page'] = 'change_password';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function checkValidationChangePass() {
        $this->form_validation->set_rules('password', 'New Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim|required|matches[password]');
        if ($this->form_validation->run($this) == true) {
            if ($this->gr_auth->reset_password($this->session->userdata('user_id'), $this->input->post('password'))) {
                $this->session->set_flashdata('success', 'Password Updated Successfully');
                redirect(site_url('admin/home'));
            }
        }
    }

    public function sliders() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/sliders');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadSliders(0, 0, TRUE, $content);

        $this->data['sliders'] = $this->admin_model->loadSliders($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/sliders.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_slider() {

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'English Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');
            $this->form_validation->set_rules('en_title_large', 'English Large Title', 'trim|required');
            $this->form_validation->set_rules('ar_title_large', 'Arabic Large Title', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/sliders/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/add_slider');
                    } else {
                        $image = $this->upload->data('file_name');
                    }
                    $data = array(
                        'en_title' => $this->input->post('en_title'),
                        'ar_title' => $this->input->post('ar_title'),
                        'en_title_large' => $this->input->post('en_title_large'),
                        'ar_title_large' => $this->input->post('ar_title_large'),
                        'image' => $image
                        );
                    $this->admin_model->insertRow($data, 'sliders');

                    $this->session->set_flashdata('success', 'Slider added Successfully');
                    redirect('admin/sliders');
                } else {
                    $this->session->set_flashdata('error', 'Image is mandatory');
                    redirect('admin/add_slider');
                }
            }
        }
        $data['view_page'] = 'admin/add_slider';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_slider($id) {

        $get_slider = $this->admin_model->getOneWhere(array('id' => $id), 'sliders');
        if (!$get_slider) {
            redirect(site_url());
        } else {
            unlink('uploads/sliders/' . $get_slider->image);
            $this->admin_model->deleteWhere(array('id' => $id), 'sliders');
            $this->session->set_flashdata('success', 'Slider Deleted Successfully');
            redirect(site_url('admin/sliders'));
        }
    }

    public function gallery() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/gallery');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadGallery(0, 0, TRUE, $content);

        $this->data['gallery'] = $this->admin_model->loadGallery($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/gallery.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_gallery() {

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'English Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/gallery/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/add_gallery');
                    } else {
                        $image = $this->upload->data('file_name');
                    }
                    $data = array(
                        'en_title' => $this->input->post('en_title'),
                        'ar_title' => $this->input->post('ar_title'),
                        'image' => $image
                        );
                    $this->admin_model->insertRow($data, 'gallery');

                    $this->session->set_flashdata('success', 'Gallery added Successfully');
                    redirect('admin/gallery');
                } else {
                    $this->session->set_flashdata('error', 'Image is mandatory');
                    redirect('admin/add_gallery');
                }
            }
        }
        $data['view_page'] = 'admin/add_gallery';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_gallery($id) {

        $get_gallery = $this->admin_model->getOneWhere(array('id' => $id), 'gallery');
        if (!$get_gallery) {
            redirect(site_url());
        } else {
            unlink('uploads/gallery/' . $get_gallery->image);
            $this->admin_model->deleteWhere(array('id' => $id), 'gallery');
            $this->session->set_flashdata('success', 'Gallery Deleted Successfully');
            redirect(site_url('admin/gallery'));
        }
    }

    public function event_gallery() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/event_gallery');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadEventGallery(0, 0, TRUE, $content);

        $this->data['event_gallery'] = $this->admin_model->loadEventGallery($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/event_gallery.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_event_gallery() {

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'English Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/event_gallery/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/add_event_gallery');
                    } else {
                        $image = $this->upload->data('file_name');
                    }
                    $data = array(
                        'en_title' => $this->input->post('en_title'),
                        'ar_title' => $this->input->post('ar_title'),
                        'image' => $image
                        );
                    $this->admin_model->insertRow($data, 'event_gallery');

                    $this->session->set_flashdata('success', 'Event Gallery added Successfully');
                    redirect('admin/event_gallery');
                } else {
                    $this->session->set_flashdata('error', 'Image is mandatory');
                    redirect('admin/add_event_gallery');
                }
            }
        }
        $data['view_page'] = 'admin/add_event_gallery';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_event_gallery($id) {

        $get_event_gallery = $this->admin_model->getOneWhere(array('id' => $id), 'event_gallery');
        if (!$get_event_gallery) {
            redirect(site_url());
        } else {
            unlink('uploads/event_gallery/' . $get_event_gallery->image);
            $this->admin_model->deleteWhere(array('id' => $id), 'event_gallery');
            $this->session->set_flashdata('success', 'Event Gallery Deleted Successfully');
            redirect(site_url('admin/event_gallery'));
        }
    }

    public function video_gallery() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/video_gallery');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadVideoGallery(0, 0, TRUE, $content);

        $this->data['video_gallery'] = $this->admin_model->loadVideoGallery($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/video_gallery.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_video_gallery() {

        if ($this->input->post()) {
            $this->form_validation->set_rules('link', 'Link', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                $data = array(
                    'link' => $this->input->post('link')
                    );
                $this->admin_model->insertRow($data, 'video_gallery');

                $this->session->set_flashdata('success', 'Video Gallery added Successfully');
                redirect('admin/video_gallery');
            }
        }
        $data['view_page'] = 'admin/add_video_gallery';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_video_gallery($id) {

        $get_video_gallery = $this->admin_model->getOneWhere(array('id' => $id), 'video_gallery');
        if (!$get_video_gallery) {
            redirect(site_url());
        } else {
            $this->admin_model->deleteWhere(array('id' => $id), 'video_gallery');
            $this->session->set_flashdata('success', 'Video Gallery Deleted Successfully');
            redirect(site_url('admin/video_gallery'));
        }
    }

    public function menu() {

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_home', 'English Home', 'required');
            $this->form_validation->set_rules('en_about', 'English About', 'required');
            $this->form_validation->set_rules('en_reservation', 'English Rooms & Rates', 'required');
            $this->form_validation->set_rules('en_gallery', 'English Gallery', 'required');
            $this->form_validation->set_rules('en_contact', 'English Contact', 'required');
            $this->form_validation->set_rules('en_services', 'English Services', 'required');
            $this->form_validation->set_rules('en_events', 'English Events', 'required');
            $this->form_validation->set_rules('ar_home', 'Arabic Home', 'required');
            $this->form_validation->set_rules('ar_about', 'Arabic About', 'required');
            $this->form_validation->set_rules('ar_reservation', 'Arabic Reservation', 'required');
            $this->form_validation->set_rules('ar_gallery', 'Arabic Gallery', 'required');
            $this->form_validation->set_rules('ar_contact', 'Arabic Contact', 'required');
            $this->form_validation->set_rules('ar_services', 'Arabic Services', 'required');
            $this->form_validation->set_rules('ar_events', 'Arabic Events', 'required');
            if ($this->form_validation->run() == true) {
                $data = array(
                    'en_home' => $this->input->post('en_home'),
                    'en_about' => $this->input->post('en_about'),
                    'en_reservation' => $this->input->post('en_reservation'),
                    'en_services' => $this->input->post('en_services'),
                    'en_events' => $this->input->post('en_events'),
                    'en_gallery' => $this->input->post('en_gallery'),
                    'en_contact' => $this->input->post('en_contact'),
                    'ar_home' => $this->input->post('ar_home'),
                    'ar_about' => $this->input->post('ar_about'),
                    'ar_reservation' => $this->input->post('ar_reservation'),
                    'ar_services' => $this->input->post('ar_services'),
                    'ar_events' => $this->input->post('ar_events'),
                    'ar_gallery' => $this->input->post('ar_gallery'),
                    'ar_contact' => $this->input->post('ar_contact')
                    );
                $this->admin_model->updateWhere(array('status' => 1), $data, 'menu');
                $this->session->set_flashdata('success', 'Menu Contents Updated Successfully');
                redirect(site_url('admin/menu'));
            }
            else{
                debug(validation_errors());
            }
        }

        $this->data['menu'] = $this->admin_model->getOneWhere(array('status' => 1), 'menu');

        $data['view_page'] = 'admin/menu.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function about_us() {

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'Title', 'required');
            $this->form_validation->set_rules('en_desc', 'Description', 'required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'required');
            if ($this->form_validation->run() == true) {
                $data = array(
                    'en_title' => $this->input->post('en_title'),
                    'ar_title' => $this->input->post('ar_title'),
                    'en_desc' => $this->input->post('en_desc'),
                    'ar_desc' => $this->input->post('ar_desc'),
                    );
                $this->admin_model->updateWhere(array('status' => 1), $data, 'about_us');

                $this->session->set_flashdata('success', 'About Us Updated Successfully');
                redirect(site_url('admin/about_us'));
            }
        }

        $aboutUs = $this->admin_model->getAll('about_us');
        $this->data['about_us'] = $aboutUs[0];

        $data['view_page'] = 'admin/about_us.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function privacy() {

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_content', 'Content', 'required');
            $this->form_validation->set_rules('ar_content', 'Arabic Content', 'required');
            if ($this->form_validation->run() == true) {
                $data = array(
                    'en_content' => $this->input->post('en_content'),
                    'ar_content' => $this->input->post('ar_content')
                    );
                $this->admin_model->updateWhere(array('status' => 1), $data, 'privacy');
                $this->session->set_flashdata('success', 'Privacy Policy Updated Successfully');
                redirect(site_url('admin/privacy'));
            }
        }

        $privacy = $this->admin_model->getAll('privacy');
        $this->data['privacy'] = $privacy[0];

        $data['view_page'] = 'admin/privacy_policy.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function upload_files($folder = 'services') {
        if (empty($_FILES['file']['name'])) {

        } else if ($_FILES['file']['error'] == 0) {
            $filetype = NULL;
            //upload and update the file
            $config['upload_path'] = './uploads/' . $folder . '/';
            $config['max_size'] = '102428800';
            $type = $_FILES['file']['type'];
            switch ($type) {
                case 'image/gif':
                case 'image/jpg':
                case 'image/png':
                case 'image/jpeg': {
                    $filetype = 0;
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    break;
                }
            }
            $config['overwrite'] = false;
            $config['remove_spaces'] = true;
            if (!file_exists('./uploads/' . $folder)) {
                if (!mkdir('./uploads/' . $folder . '/', 0755, TRUE)) {

                }
            }
            $microtime = microtime(TRUE) * 10000;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload('file', $microtime)) {
                echo json_encode(array('type' => $filetype, 'path' => 'uploads/' . $folder . '/' . $this->upload->file_name));
            }
        }
        exit;
    }

    public function about_us_history() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/about_us_history/');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadAboutUsHistory(0, 0, TRUE, $content);

        $this->data['about_us_history'] = $this->admin_model->loadAboutUsHistory($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/about_us_history';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_about_us_history() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('en_year', 'Year', 'trim|required');
            $this->form_validation->set_rules('ar_year', 'Arabic Year', 'trim|required');
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');
            $this->form_validation->set_rules('en_desc', 'Description', 'trim|required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                $data = array(
                    'en_year' => $this->input->post('en_year'),
                    'ar_year' => $this->input->post('ar_year'),
                    'en_title' => $this->input->post('en_title'),
                    'ar_title' => $this->input->post('ar_title'),
                    'en_desc' => $this->input->post('en_desc'),
                    'ar_desc' => $this->input->post('ar_desc'),
                    );
                $this->admin_model->insertRow($data, 'about_us_history');

                $this->session->set_flashdata('success', 'About Us History added Successfully');
                redirect('admin/about_us_history');
            }
        }

        $data['view_page'] = 'admin/add_about_us_history';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function edit_about_us_history($id) {

        $history = $this->admin_model->getOneWhere(array('id' => $id), 'about_us_history');
        if (!$history) {
            redirect('admin/about_us_history');
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_title', 'Year', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Year', 'trim|required');
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');
            $this->form_validation->set_rules('en_desc', 'Description', 'trim|required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                $data = array(
                    'en_year' => $this->input->post('en_year') ? $this->input->post('en_year') : $history->en_year,
                    'ar_year' => $this->input->post('ar_year') ? $this->input->post('ar_year') : $history->ar_year,
                    'en_title' => $this->input->post('en_title') ? $this->input->post('en_title') : $history->en_title,
                    'ar_title' => $this->input->post('ar_title') ? $this->input->post('ar_title') : $history->ar_title,
                    'en_desc' => $this->input->post('en_desc') ? $this->input->post('en_desc') : $history->en_desc,
                    'ar_desc' => $this->input->post('ar_desc') ? $this->input->post('ar_desc') : $history->ar_desc,
                    );
                $this->admin_model->updateWhere(array('id' => $id), $data, 'about_us_history');

                $this->session->set_flashdata('success', 'About Us History updated Successfully');
                redirect('admin/about_us_history');
            }
        }

        $this->data['about_us_history'] = $history;

        $data['view_page'] = 'admin/edit_about_us_history';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_about_us_history($id) {
        $about_us_history = $this->admin_model->getOneWhere(array('id' => $id), 'about_us_history');
        if (!$about_us_history) {
            redirect(site_url());
        } else {
            $this->admin_model->deleteWhere(array('id' => $id), 'about_us_history');
            $this->session->set_flashdata('success', 'About Us History Deleted Successfully');
            redirect(site_url('admin/about_us_history'));
        }
    }

    public function services() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/services/');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadServices(0, 0, TRUE, $content);

        $this->data['services'] = $this->admin_model->loadServices($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/services';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_service() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('en_name', 'Name', 'trim|required');
            $this->form_validation->set_rules('ar_name', 'Arabic Name', 'trim|required');
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');
            $this->form_validation->set_rules('en_desc', 'Description', 'trim|required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/services/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/add_service');
                    } else {
                        $image = $this->upload->data('file_name');
                    }
                    $data = array(
                        'en_name' => $this->input->post('en_name'),
                        'ar_name' => $this->input->post('ar_name'),
                        'en_title' => $this->input->post('en_title'),
                        'ar_title' => $this->input->post('ar_title'),
                        'en_desc' => $this->input->post('en_desc'),
                        'ar_desc' => $this->input->post('ar_desc'),
                        'image' => $image
                        );
                    $service = $this->admin_model->insertRow($data, 'services');

                    $this->session->set_flashdata('success', 'Service added Successfully');
                    redirect('admin/services');
                } else {
                    $this->session->set_flashdata('error', 'Image is mandatory');
                    redirect('admin/add_service');
                }
            }
        }

        $data['view_page'] = 'admin/add_service';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function edit_service($id) {

        $service = $this->admin_model->getOneWhere(array('id' => $id), 'services');
        if (!$service) {
            redirect('admin/services');
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_name', 'Name', 'trim|required');
            $this->form_validation->set_rules('ar_name', 'Arabic Name', 'trim|required');
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');
            $this->form_validation->set_rules('en_desc', 'Description', 'trim|required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/services/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/services');
                    } else {
                        unlink('uploads/services/' . $service->image);
                        $image = $this->upload->data('file_name');
                    }
                } else {
                    $image = $service->image;
                }
                $data = array(
                    'en_name' => $this->input->post('en_name') ? $this->input->post('en_name') : $service->en_name,
                    'ar_name' => $this->input->post('ar_name') ? $this->input->post('ar_name') : $service->ar_name,
                    'en_title' => $this->input->post('en_title') ? $this->input->post('en_title') : $service->en_title,
                    'ar_title' => $this->input->post('ar_title') ? $this->input->post('ar_title') : $service->ar_title,
                    'en_desc' => $this->input->post('en_desc') ? $this->input->post('en_desc') : $service->en_desc,
                    'ar_desc' => $this->input->post('ar_desc') ? $this->input->post('ar_desc') : $service->ar_desc,
                    'image' => $image
                    );
                $this->admin_model->updateWhere(array('id' => $id), $data, 'services');

                $this->session->set_flashdata('success', 'Service updated Successfully');
                redirect('admin/services');
            }
        }

        $this->data['service'] = $service;

        $data['view_page'] = 'admin/edit_service';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_service($id) {
        $service = $this->admin_model->getOneWhere(array('id' => $id), 'services');
        if (!$service) {
            redirect(site_url());
        } else {
            unlink('uploads/services/' . $service->image);
            $this->admin_model->deleteWhere(array('id' => $id), 'services');

            $this->session->set_flashdata('success', 'Service Deleted Successfully');
            redirect(site_url('admin/services'));
        }
    }

    public function events() {
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/events/');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadEvents(0, 0, TRUE, $content);

        $this->data['events'] = $this->admin_model->loadEvents($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/events';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function add_event() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('en_name', 'Name', 'trim|required');
            $this->form_validation->set_rules('ar_name', 'Arabic Name', 'trim|required');
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');
            $this->form_validation->set_rules('en_desc', 'Description', 'trim|required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/events/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/add_event');
                    } else {
                        $image = $this->upload->data('file_name');
                    }
                    $data = array(
                        'en_name' => $this->input->post('en_name'),
                        'ar_name' => $this->input->post('ar_name'),
                        'en_title' => $this->input->post('en_title'),
                        'ar_title' => $this->input->post('ar_title'),
                        'en_desc' => $this->input->post('en_desc'),
                        'ar_desc' => $this->input->post('ar_desc'),
                        'image' => $image
                        );
                    $event = $this->admin_model->insertRow($data, 'events');

                    $this->session->set_flashdata('success', 'Event added Successfully');
                    redirect('admin/events');
                } else {
                    $this->session->set_flashdata('error', 'Image is mandatory');
                    redirect('admin/add_event');
                }
            }
        }

        $data['view_page'] = 'admin/add_event';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function edit_event($id) {

        $event = $this->admin_model->getOneWhere(array('id' => $id), 'events');
        if (!$event) {
            redirect('admin/events');
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules('en_name', 'Name', 'trim|required');
            $this->form_validation->set_rules('ar_name', 'Arabic Name', 'trim|required');
            $this->form_validation->set_rules('en_title', 'Title', 'trim|required');
            $this->form_validation->set_rules('ar_title', 'Arabic Title', 'trim|required');
            $this->form_validation->set_rules('en_desc', 'Description', 'trim|required');
            $this->form_validation->set_rules('ar_desc', 'Arabic Description', 'trim|required');

            if ($this->form_validation->run() != FALSE) {

                if (isset($_FILES) && isset($_FILES["uploadfile"]['tmp_name']) && $_FILES["uploadfile"]['tmp_name']) {
                    $file = $_FILES["uploadfile"]['tmp_name'];
                    $path = './uploads/events/';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, TRUE);
                    }
                    $file_type = 'gif|jpg|jpeg|png';
                    $config = $this->set_upload_options($path, $file_type);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('uploadfile')) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('admin/events');
                    } else {
                        unlink('uploads/events/' . $event->image);
                        $image = $this->upload->data('file_name');
                    }
                } else {
                    $image = $event->image;
                }
                $data = array(
                    'en_name' => $this->input->post('en_name') ? $this->input->post('en_name') : $event->en_name,
                    'ar_name' => $this->input->post('ar_name') ? $this->input->post('ar_name') : $event->ar_name,
                    'en_title' => $this->input->post('en_title') ? $this->input->post('en_title') : $event->en_title,
                    'ar_title' => $this->input->post('ar_title') ? $this->input->post('ar_title') : $event->ar_title,
                    'en_desc' => $this->input->post('en_desc') ? $this->input->post('en_desc') : $event->en_desc,
                    'ar_desc' => $this->input->post('ar_desc') ? $this->input->post('ar_desc') : $event->ar_desc,
                    'image' => $image
                    );
                $this->admin_model->updateWhere(array('id' => $id), $data, 'events');

                $this->session->set_flashdata('success', 'Event updated Successfully');
                redirect('admin/events');
            }
        }

        $this->data['event'] = $event;

        $data['view_page'] = 'admin/edit_event';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_event($id) {
        $event = $this->admin_model->getOneWhere(array('id' => $id), 'events');
        if (!$event) {
            redirect(site_url());
        } else {
            unlink('uploads/events/' . $event->image);
            $this->admin_model->deleteWhere(array('id' => $id), 'events');

            $this->session->set_flashdata('success', 'Event Deleted Successfully');
            redirect(site_url('admin/events'));
        }
    }

    public function manage_highlight() {
        $id = $this->input->post('id');
        $this->admin_model->updateWhere(array('id' => $id), array('highlight' => $this->input->post('val')), 'services');
        echo true;
    }

    public function contact_list(){
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/contact_list');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadContacts(0, 0, TRUE, $content);

        $this->data['contact_list'] = $this->admin_model->loadContacts($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/contact_list.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function reservation_list(){
        $content = $this->input->get('content');

        $perpage = 10;
        $base_url = site_url('admin/reservation_list');
        $uri_segment = 3;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        if ($this->input->get('search')) {
            $page = 1;
            unset($_GET['search']);
        }

        $total = $this->admin_model->loadReservations(0, 0, TRUE, $content);

        $this->data['reservation_list'] = $this->admin_model->loadReservations($perpage, $page, FALSE, $content);
        $this->data['pagination'] = $this->paginate($perpage, $total, $base_url, $uri_segment, $class = "");
        $this->data['page'] = $page;
        $this->data['perpage'] = $perpage;

        $data['view_page'] = 'admin/reservation_list.php';
        $data['data'] = $this->data;
        $this->load->view('template', $data);
    }

    public function delete_contact_list($id) {
        $contact_list = $this->admin_model->getOneWhere(array('id' => $id), 'contact');
        if (!$contact_list) {
            redirect(site_url());
        } else {
            $this->admin_model->updateWhere(array('id' => $id), array('status' => 0), 'contact');
            $this->session->set_flashdata('success', 'Contact Deleted Successfully');
            redirect(site_url('admin/contact_list'));
        }
    }

    public function delete_reservation_list($id) {
        $reservation_list = $this->admin_model->getOneWhere(array('id' => $id), 'reservation');
        if (!$reservation_list) {
            redirect(site_url('admin/contact_list'));
        } else {
            $this->admin_model->updateWhere(array('id' => $id), array('status' => 0), 'reservation');
            $this->session->set_flashdata('success', 'Reservation Deleted Successfully');
            redirect(site_url('admin/reservation_list'));
        }
    }

}
