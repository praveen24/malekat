<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gr_auth {

    private $_user_id = null;
    private $CI = NULL;
    private $store_salt = true;
    private $_system = true;
    private $_customer_user_id = null;
    private $salt_length = 5;
    private $_error = '';
    private $_login_hash = '';
    private $_table_name = NULL;

    /**
     * @param $tablename
     * @param bool $system
     */
    public function __construct($tablename = 'user') {
        $this->_table_name = $tablename;

        $this->CI = &get_instance();

        $this->CI->load->helper('cookie');
        if ($tablename === 'parent') {
            $this->_system = false;
            $this->_login_hash = $this->CI->session->userdata('login_hash_parent');
        } else {
            $this->_login_hash = $this->CI->session->userdata('login_hash_user');
        }

        if (md5($this->_table_name) == $this->_login_hash) {

            $session_user_id = $session_customer_user_id = NULL;

            if ($this->_system) {
                $session_user_id = $this->CI->session->userdata('user_id');
            } else {
                $session_customer_user_id = $this->CI->session->userdata('parent_user_id');
            }

            $user = $this->CI->db->get_where('user', array('id' => ($session_user_id) ? $session_user_id : $session_customer_user_id));

            if ($user->num_rows() === 1) {
                $this->_user_id = ($this->_system) ? $session_user_id : null;
                $this->_customer_user_id = ($this->_system) ? null : $session_customer_user_id;
            }

            if (!$this->logged_in() && get_cookie('identity') && get_cookie('remember_code'))
                $this->login_remembered_user();
        }
    }

    /**
     * Set login mode to system mode or parent's portal
     * @param null $mode
     */
    public function set_login_mode($mode = null) {
        if (strcmp($mode, 'system') != 0) {
            $this->_system = false;
        }
    }

    /**
     * Identify the login mode
     * @return bool
     */
    public function get_login_mode() {
        return $this->_system ? 'system' : 'client';
    }

    public function login($identity, $pass_word, $remember = FALSE) {
        $password = $pass_word;
        if (empty($identity) || empty($password)) {
            $this->_error = 'Invalid Username/password.';
            return FALSE;
        }

        $query = $this->CI->db->select('*')
                ->from($this->_table_name)
                ->where('username', $identity)
                ->get();



        if ($query->num_rows() === 1) {
            $user = $query->row();
            if (password_verify($password, $user->password)) {

                $this->set_session($user->id, $user->is_admin);

                if ($this->_system) {
                    $this->_user_id = $user->id;
                } else {
                    $this->_customer_user_id = $user->id;
                }
                if ($remember) {
                    $this->remember_user($user->id, $identity, $user->password);
                }
                return TRUE;
            } else {
                $this->_error = "Invalid Username/Password";
                return FALSE;
            }
        }
        $this->_error = 'Invalid Username/password.';
        return FALSE;
    }

    public function forgot_password($identity) {

        if (empty($identity)) {
            $this->_error = 'Invalid Username/Email.';
            return FALSE;
        }
        $query = $this->CI->db->select('user.*,login.forgotten_password_code')
                ->from('user')
                ->join('login', 'login.user_id=user.id')
                ->where('user.email', $identity)
                ->limit(1)
                ->get();

        if ($query->num_rows() === 1) {
            $user = $query->row();

            if ($user->active == 0) {
                $this->_error = 'Account is inactive. Please contact Administrator for more details.';
                return FALSE;
            }

            $random_string = $this->random_string(10);
            $forgotten_password_code = md5($random_string);
            $user->forgotten_password_code = $forgotten_password_code;

            return $this->CI->db->update($this->_table_name, array('forgotten_password_code' => $forgotten_password_code), array('user_id' => $user->id)) ? $user : FALSE;
        }

        $this->_error = 'Invalid Username/Email.';
        return FALSE;
    }

    public function password_forgotten_user($user_id) {

        if (empty($user_id)) {
            $this->_error = 'Invalid Request.';
            return FALSE;
        }

        $query = $this->CI->db->select('user.*,login.forgotten_password_code')
                ->from('user')
                ->join('login', 'login.user_id=user.id')
                ->where('user.id', $user_id)
                ->limit(1)
                ->get();

        if ($query->num_rows() === 1) {
            $user = $query->row();
            if ($user->active == 0) {
                $this->_error = 'Account is inactive. Please contact Administrator for more details.';
                return FALSE;
            } else
                return $user;
        }

        $this->_error = 'Invalid Request.';
        return FALSE;
    }

    public function reset_password($user_id, $password) {

        return $this->CI->db->update('user', array(
            'password' => password_hash($password,1)
        ), array('id' => $user_id));
    }

    public function set_session($user_id, $is_admin = 0) {
        $this->CI->session->set_userdata(array(
            'user_id' => $user_id,
            'is_admin' => $is_admin
            ));
        $this->CI->session->set_userdata(array('login_hash_user' => md5($this->_table_name)));
    }

    public function logout() {

        if ($this->_system) {
            $this->CI->session->unset_userdata('user_id');
            $this->_user_id = null;
        } else {
            $this->CI->session->unset_userdata('customer_user_id');
            $this->_customer_user_id = null;
        }
        if (get_cookie('remember_code')) {
            delete_cookie('identity');
            delete_cookie('remember_code');
        }
    }

    public function logged_in() {
        return ($this->_system) ? !is_null($this->_user_id) : !is_null($this->_customer_user_id);
    }

    public function customer_logged_in() {
//        var_dump($this->_system);
        return ($this->_system) ? !is_null($this->_user_id) : !is_null($this->_customer_user_id);
    }

    public function user_id() {

        return $this->_user_id;
    }

    /**
     * Get parent_id
     * @return null
     */
    public function customer_user_id() {

        return $this->_customer_user_id;
    }

    public function hash_password($password, $salt = false) {

        return password_hash($password, 1);
    }

    public function remember_user($id, $identity, $password) {

        if (!$id)
            return FALSE;

        $salt = sha1($password);

        $this->CI->db->update($this->_table_name, array('remember_code' => $salt), array('id' => $id));

        if ($this->CI->db->affected_rows() > -1) {
            $expire = (60 * 60 * 24 * 30);

            set_cookie(array(
                'name' => 'identity',
                'value' => $identity,
                'expire' => $expire
            ));

            set_cookie(array(
                'name' => 'remember_code',
                'value' => $salt,
                'expire' => $expire
            ));
            return TRUE;
        }
        return FALSE;
    }

    public function login_remembered_user() {

        if (!get_cookie('identity') || !get_cookie('remember_code'))
            return FALSE;

        $query = $this->CI->db->select('id, password')
                ->where('username', get_cookie('identity'))
                ->or_where('email', get_cookie('identity'))
                ->where('remember_code', get_cookie('remember_code'))
                ->limit(1)
                ->get($this->_table_name);

        if ($query->num_rows() == 1) {
            $user = $query->row();

            $this->set_session($user->id, $user->is_admin);

            $this->remember_user($user->id, get_cookie('identity'), $user->password);

            return TRUE;
        }

        return FALSE;
    }

    public function error() {
        return $this->_error;
    }

    public function activate($user_id, $activation_code) {

        $query = $this->CI->db->select('active')
                        ->where('id', $user_id)
                        ->where('activation_code', $activation_code)
                        ->limit(1)->get($this->_table_name);


        if ($query->num_rows() == 1) {
            $this->set_session($user_id);
            $this->_user_id = $user_id;

            return $this->CI->db->where('id', $user_id)->update($this->_table_name, array('active' => 1, 'activated_at' => date(DATE_FORMAT)));
        }
        return false;
    }

    function check_center_status() {

        $userId = $this->_user_id;

        $roleDetails = $this->CI->db->get_where(IMS_DB_PREFIX . 'user_role', array('user_id' => $userId))->row();

        if ($roleDetails->role_id == 1) {
            return 1;
        } else {
            $centerStatus = $this->CI->db->select('b.status')
                            ->from(IMS_DB_PREFIX . 'user as a')
                            ->join(IMS_DB_PREFIX . 'user_role as r', 'r.user_id = a.id')
                            ->join(IMS_DB_PREFIX . 'center as b', 'b.id = r.center_id')
                            ->where('a.id', $userId)->get()->row();
            return $centerStatus ? $centerStatus->status : false;
        }
    }

    public function direct_login($identity, $pass_word, $remember = FALSE) {
        $password = $pass_word;
        if (empty($identity) || empty($password)) {
            $this->_error = 'Invalid Username/password.';
            return FALSE;
        }

        $query = $this->CI->db->select('*')
                ->from($this->_table_name)
                ->where('username', $identity)
                ->or_where('email', $identity)
                ->limit(1)
                ->get();

        if ($this->is_time_locked_out($identity)) {
            $this->_error = 'Login timeout';

            return FALSE;
        }

        if ($query->num_rows() === 1) {
            $user = $query->row();

            if ($password == $user->password) {
                if ($user->active == 0) {
                    $this->_error = 'Please retrieve the email that was sent to your registered email address and click on the link to activate this login account. Thank you.';
                    return FALSE;
                }

                $this->set_session($user->id);

                $this->_user_id = $user->id;

                $this->clear_login_attempts($identity);

                if ($remember) {
                    $this->remember_user($user->id, $identity, $user->password);
                }
                return TRUE;
            }
        }

        $this->increase_login_attempts($identity);

        $this->_error = 'Invalid Username/password.';
        return FALSE;
    }

    public function getuserFromId($user_id, $code) {
        if ($code != NULL) {
            return $this->CI->db->get_where('user', array('id' => $user_id, 'activation_code' => $code))->row();
        } else {
            return array();
        }
    }

    public function login_with_facebook($array) {
        if (empty($array)) {
            $this->_error = 'Login Failed.';
            return FALSE;
        }
        $this->system = false;
        $this->CI->session->set_userdata(array('origin_centre_id' => null));
        $checkuser = $this->CI->db->select('*')
                ->from('customer')
                ->or_where('email', $array['email'])
                ->limit(1)
                ->get();
        if ($checkuser->num_rows() === 1) {
            $user = $checkuser->row();
            if ($user->active == 0) {
                $this->_error = 'Your account has not been activated.  Please retrieve the confirmation email that is sent to your registered email address.  Activate your account by following the instruction given in the confirmation email.';
                return FALSE;
            }
            $this->set_session($user->id);
            $this->_customer_user_id = $user->id;
            return TRUE;
        } else {
            $arrayinsert = array(
                'first_name' => $array['first_name'],
                'last_name' => $array['last_name'],
                'email' => $array['email'],
                'username' => $array['email'],
                'password' => $array['accesstoken'],
                'active' => 1,
                'activated_at' => date(DATE_FORMAT),
                'avatar' => 'http://graph.facebook.com/' . $array['id'] . '/picture?type=large',
                'created_at' => date(DATE_FORMAT),
                'created_at' => date(DATE_FORMAT)
            );
            $this->CI->db->insert('customer', $arrayinsert);
            $user_id = $this->CI->db->insert_id();
            $this->set_session($user_id);
            $this->_customer_user_id = $user_id;
            return TRUE;
        }

        $this->_error = 'Invalid Username/password.';
        return FALSE;
    }

    public function random_string($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < $length; $i++) {
            $randstring = $characters[rand(0, strlen($characters))];
        }
        return $randstring;
    }

}
